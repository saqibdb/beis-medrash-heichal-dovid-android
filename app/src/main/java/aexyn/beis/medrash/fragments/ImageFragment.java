package aexyn.beis.medrash.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.chrisbanes.photoview.PhotoView;
import aexyn.beis.medrash.R;
import aexyn.beis.medrash.model.EventModel;

public class ImageFragment extends Fragment {
    EventModel eventModel;
    View rootView;

    public ImageFragment() {
    }

    public static ImageFragment newInstance(EventModel eventModel) {
        ImageFragment fragment = new ImageFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("eventModel", eventModel);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            eventModel = getArguments().getParcelable("eventModel");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_image, container, false);

        PhotoView imageEvent = (PhotoView) rootView.findViewById(R.id.imageEvent);
        final ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);

        TextView description=(TextView)rootView.findViewById(R.id.description);
        if(!eventModel.getDescription().equals("")) {
            description.setText(eventModel.getDescription());
        }
        try {
            String imgURl = eventModel.getImage();
            if (imgURl.equals("http://hd.welikeyour.biz/uploads/event/") || imgURl == null) {
                progressBar.setVisibility(View.GONE);
            } else {
                Glide.with(getActivity()).load(eventModel.getImage())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                progressBar.setVisibility(View.GONE);
                                return false;
                            }
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                progressBar.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(imageEvent);
            }
        }catch (Exception e){}
        return rootView;
    }
}
