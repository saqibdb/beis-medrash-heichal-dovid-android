package aexyn.beis.medrash.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.MailTo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import aexyn.beis.medrash.R;
import im.delight.android.webview.AdvancedWebView;

public class Contact_Fragment extends BaseFragment implements AdvancedWebView.Listener{
    private static final String ARG_TYPE = "type";
    String type;
    TextView tvRefresh;
    AdvancedWebView mWebView;

    public static Contact_Fragment newInstance(String type) {
        Contact_Fragment fragment = new Contact_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    public Contact_Fragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getString(ARG_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_contact_, container, false);
        tvRefresh = (TextView) view.findViewById(R.id.tvRefresh);

        mWebView = (AdvancedWebView) view.findViewById(R.id.Contact_webview);
        mWebView.setListener(getActivity(), this);
        mWebView.getSettings().setAllowContentAccess(true);
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.getSettings().setJavaScriptEnabled(true);

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("mailto:")) {
                    MailTo mt = MailTo.parse(url);
                    sendEmail(mt.getTo());

                } else {

                    view.loadUrl(url);
                }
                return true;
            }
        });

        return view;
    }
    public void loadDataInWebView(){
        mWebView.loadData(getreadPref().getStringValue("zemanim"), "text/html", "UTF-8");
    }

    public void sendEmail(String Email_ID){
        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{Email_ID});
        email.putExtra(Intent.EXTRA_SUBJECT, "");
        email.putExtra(Intent.EXTRA_TEXT, "");
        email.setType("message/rfc822");
        startActivity(Intent.createChooser(email, "Create an email"));
    }

    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();
        loadDataInWebView();
        mWebView.onResume();
    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        mWebView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mWebView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        showProgressBar();
    }

    @Override
    public void onPageFinished(String url) {
        hideProgressBar();
        enableMenu();
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        enableMenu();
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }
    @Override
    public void onExternalPageRequest(String url) {

    }
}
