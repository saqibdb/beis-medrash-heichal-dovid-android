package aexyn.beis.medrash.fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import aexyn.beis.medrash.R;

public class DonateFragment extends BaseFragment implements View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    View rootView;
    TextView tvEmail;
    private String mParam1;
    private String mParam2;

    public DonateFragment() {
    }

    public static DonateFragment newInstance(String param1, String param2) {
        DonateFragment fragment = new DonateFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        enableMenu();
        rootView = inflater.inflate(R.layout.fragment_donate, container, false);
        rootView.findViewById(R.id.imgChase).setOnClickListener(this);
        rootView.findViewById(R.id.imgPaypal).setOnClickListener(this);
        tvEmail = (TextView) rootView.findViewById(R.id.tvEmail);
        tvEmail.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgChase:
            case R.id.imgPaypal:

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.bm-hd.com/payment.php"));
                /*https://www.chase.com/business-banking/online-banking/quickpay*/
                startActivity(browserIntent);
                break;
            case R.id.tvEmail:
                copyToClipboard(tvEmail.getText().toString());
                break;
        }
    }

    public void copyToClipboard(String copyText) {
        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("url", copyText);
        clipboard.setPrimaryClip(clip);
        Toast toast = Toast.makeText(getActivity(), "Email copied", Toast.LENGTH_SHORT);
        toast.show();
    }

}
