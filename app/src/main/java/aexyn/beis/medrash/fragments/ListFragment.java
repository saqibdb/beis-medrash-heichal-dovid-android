package aexyn.beis.medrash.fragments;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import aexyn.beis.medrash.BuildConfig;
import aexyn.beis.medrash.R;
import aexyn.beis.medrash.adapters.NotificationAdapter;
import aexyn.beis.medrash.adapters.ShiurimAdapter;
import aexyn.beis.medrash.asynctask.AsyncRequest;
import aexyn.beis.medrash.db.NotificationStorage;
import aexyn.beis.medrash.db.ShiurimStorage;
import aexyn.beis.medrash.model.NotificationModel;
import aexyn.beis.medrash.model.ShiurimModel;
import aexyn.beis.medrash.utils.Constants;
import aexyn.beis.medrash.utils.NetworkUtils;
import aexyn.beis.medrash.utils.PermissionUtility;
import io.realm.Realm;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import static aexyn.beis.medrash.utils.Constants.ACTION;
import static aexyn.beis.medrash.utils.Constants.CATEGORY_ID;
import static aexyn.beis.medrash.utils.Constants.DATA;
import static aexyn.beis.medrash.utils.Constants.NOTIFICATIONS;
import static aexyn.beis.medrash.utils.Constants.PAGE;
import static aexyn.beis.medrash.utils.Constants.SHIURIM;
import static aexyn.beis.medrash.utils.Constants.STATUS;
import static aexyn.beis.medrash.utils.Constants.TITLE;
import static aexyn.beis.medrash.utils.Constants.TOKEN;
import static aexyn.beis.medrash.utils.PermissionUtility.MY_PERMISSIONS_REQUEST_STORAGE_PERMISSION;
import static aexyn.beis.medrash.utils.Utils.showMessage;

public class ListFragment extends BaseFragment {
    private static final String ARG_TYPE = "type";
    private static final String mp3Id_Type = "mp3Id";
    public static String TAG = "ListFragment";
    View rootView;
    SwipeRefreshLayout swipeToRefresh;
    ListView listView;
    TextView tvNoData;
    NotificationAdapter notificationAdapter;
    ShiurimAdapter shiurimAdapter;
    String categoryID = "";
    int pageNum = 1;
    String title = "";
    ShiurimModel downloadModel;
    private boolean flag;
    private String type,mp3_ID;
    String position;
    public ListFragment() {
    }

    public void notifyDataChange() {
        if (shiurimAdapter != null) {
            shiurimAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getString(ARG_TYPE);
            mp3_ID= getArguments().getString(mp3Id_Type);
        }
    }

    public void setScrollListener() {
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                if (scrollState == 2)
                    flag = true;
                Log.i("Scroll State", "" + scrollState);
            }

            @Override
            public void onScroll(AbsListView lw, final int firstVisibleItem,
                                 final int visibleItemCount, final int totalItemCount) {
                if ((visibleItemCount == (totalItemCount - firstVisibleItem))
                        && flag) {
                    flag = false;
                    Log.i("Scroll", "Ended");
                    pageNum++;
                    loadData(title);
                }
                boolean enable = false;
                if(listView != null && listView.getChildCount() > 0){
                    boolean firstItemVisible = listView.getFirstVisiblePosition() == 0;
                    boolean topOfFirstItemVisible = listView.getChildAt(0).getTop() == 0;
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                swipeToRefresh.setEnabled(enable);

            }
        });

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_list, container, false);

        swipeToRefresh = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeToRefresh);
        listView = (ListView) rootView.findViewById(R.id.listView);
        tvNoData = (TextView) rootView.findViewById(R.id.tvNoData);
        tvNoData.setVisibility(View.GONE);

        if(NetworkUtils.isNetworkConnected(getActivity())){
            loadData("");
            if(type.equals(Constants.ACTIONS.getNotifications.name())){

            }else{
                setScrollListener();
            }
        }
        else {
            if (type.equals(Constants.ACTIONS.getNotifications.name())) {
                if (getreadPref().keyExixst(NOTIFICATIONS)) {
                    setList();
                } else {
                    loadData("");
                }
            } else {
                if (getreadPref().keyExixst(SHIURIM)) {
                    setList();
                } else {
                    loadData("");
                }
                setScrollListener();
            }
        }

        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageNum = 1;
                loadData(title);
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(type.equals(Constants.ACTIONS.getShiurim.name())) {
            if(shiurimAdapter != null)
                shiurimAdapter.notifyDataSetChanged();
        }
    }
    public void loadData(String title) {
        if(!NetworkUtils.isNetworkConnected(getActivity())){
            showMessage(getActivity(), getString(R.string.no_network), false);
            swipeToRefresh.post(new Runnable() {
                @Override
                public void run() {
                    swipeToRefresh.setRefreshing(false);
                }
            });
            enableMenu();
            return;
        }
        this.title = title;
        disableMenu();
        swipeToRefresh.post(new Runnable() {
            @Override
            public void run() {
                swipeToRefresh.setRefreshing(true);
            }
        });

        RequestBody formBody;

        if (type.equals(Constants.ACTIONS.getNotifications.name())) {
            formBody = new FormBody.Builder()
                    .add(TOKEN, BuildConfig.token)
                    .add(ACTION, type)
                    .build();
        } else {
            if (categoryID.isEmpty()) {
                if (title.isEmpty()) {
                    formBody = new FormBody.Builder()
                            .add(TOKEN, BuildConfig.token)
                            .add(ACTION, type)
                            .add(PAGE, String.valueOf(pageNum))
                            .build();
                } else {
                    formBody = new FormBody.Builder()
                            .add(TOKEN, BuildConfig.token)
                            .add(ACTION, type)
                            .add(TITLE, title)
                            .add(PAGE, String.valueOf(pageNum))
                            .build();
                }
            } else {
                if (title.isEmpty()) {
                    formBody = new FormBody.Builder()
                            .add(TOKEN, BuildConfig.token)
                            .add(ACTION, type)
                            .add(PAGE, String.valueOf(pageNum))
                            .add(CATEGORY_ID, categoryID)
                            .build();
                } else {
                    formBody = new FormBody.Builder()
                            .add(TOKEN, BuildConfig.token)
                            .add(ACTION, type)
                            .add(TITLE, title)
                            .add(PAGE, String.valueOf(pageNum))
                            .add(CATEGORY_ID, categoryID)
                            .build();
                }
            }
        }
        new AsyncRequest(BuildConfig.apiDomain, formBody, false, new AsyncRequest.OnTaskCompleted() {
            @Override
            public void onTaskCompleted(AsyncRequest asyncRequest, String result) {
                if (asyncRequest != null) {
                    try {

                        result = asyncRequest.get();
                        Log.e(TAG, "" + result);
                        JSONObject jsonObject1 = new JSONObject(result);
                        if (jsonObject1.getString(STATUS).equals(Constants.STATUSES.Success.name())) {
                            if(shiurimAdapter != null){
                                if(pageNum == 1) {
                                    shiurimAdapter.clear();
                                    ShiurimStorage.removeAll();
                                }
                            }
                            if(notificationAdapter != null){
                                notificationAdapter.clear();
                                NotificationStorage.removeAll();
                            }
                            JSONArray jsonArray = jsonObject1.getJSONArray(DATA);
                            if(jsonArray.length()==0){
                                listView.setOnScrollListener(new AbsListView.OnScrollListener() {
                                    @Override
                                    public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                                    }

                                    @Override
                                    public void onScroll(AbsListView lw, final int firstVisibleItem,
                                                         final int visibleItemCount, final int totalItemCount) {
                                        boolean enable = false;
                                        if(listView != null && listView.getChildCount() > 0){
                                            boolean firstItemVisible = listView.getFirstVisiblePosition() == 0;
                                            boolean topOfFirstItemVisible = listView.getChildAt(0).getTop() == 0;
                                            enable = firstItemVisible && topOfFirstItemVisible;
                                        }
                                        swipeToRefresh.setEnabled(enable);
                                    }
                                });
                            }else{
                                setScrollListener();
                            }
                            for (int i = 0; i < jsonArray.length(); i++) {

                                if (type.equals(Constants.ACTIONS.getNotifications.name())) {
                                    getSavedPref().saveBoolean(NOTIFICATIONS, true);
                                    NotificationModel notificationModel = getGson().fromJson(jsonArray.getString(i), NotificationModel.class);
                                    NotificationStorage.save(notificationModel);
                                } else {
                                    getSavedPref().saveBoolean(SHIURIM, true);

                                    ShiurimModel shiurimModel = getGson().fromJson(jsonArray.getString(i), ShiurimModel.class);
                                    ShiurimStorage.save(shiurimModel);
                                }
                            }
                            setList();
                        } else {
                            enableMenu();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        enableMenu();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                        enableMenu();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        enableMenu();
                    }
                }
                swipeToRefresh.setRefreshing(false);
            }

        }).execute();
    }

    public void setList(){
        enableMenu();
        if(type.equals(Constants.ACTIONS.getNotifications.name())) {
            notificationAdapter = new NotificationAdapter(getContext(), new ArrayList<NotificationModel>());
            listView.setAdapter(notificationAdapter);
            List<NotificationModel> list = NotificationStorage.getAll(Realm.getDefaultInstance());
            notificationAdapter.addAll(list);
            if(notificationAdapter.getCount() == 0){
                tvNoData.setVisibility(View.VISIBLE);
            }else{
                tvNoData.setVisibility(View.GONE);
            }
            listView.setOnScrollListener(null);
        }else {
            shiurimAdapter = new ShiurimAdapter(getContext(), new ArrayList<ShiurimModel>(), this);
            listView.setAdapter(shiurimAdapter);
            List<ShiurimModel> list = ShiurimStorage.getAll(Realm.getDefaultInstance());
            shiurimAdapter.addAll(list);

            for(int i=0;i<list.size();i++) {
                String mpsggetID = String.valueOf(list.get(i).getId());
                if (mpsggetID.equals(mp3_ID)) {
                    position = String.valueOf(i);
                    final SharedPreferences prefs = getActivity().getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("position", position);
                    editor.apply();
                    mp3_ID="";
                    break;
                }
            }
            if(shiurimAdapter.getCount() == 0){
                tvNoData.setVisibility(View.VISIBLE);
            }else{
                tvNoData.setVisibility(View.GONE);
            }
        }
    }
    public void downloadFile(ShiurimModel shiurimModel) {
        if(!NetworkUtils.isNetworkConnected(getActivity())){
            showMessage(getActivity(), getString(R.string.no_network), false);
            return;
        }
        downloadModel = shiurimModel;
            if (PermissionUtility.checkPermission(getActivity()))
                if (PermissionUtility.checkPermission(getActivity()))
                    new DownloadFileFromURL(shiurimModel).execute();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_STORAGE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    downloadFile(downloadModel);
                }
                break;
        }
    }
    private class DownloadFileFromURL extends AsyncTask<String, Integer, Boolean> {
        private ShiurimModel shiurimModel;
        private ProgressDialog pDialog;

        DownloadFileFromURL(ShiurimModel shiurimModel) {
            this.shiurimModel = Realm.getDefaultInstance().copyFromRealm(shiurimModel);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... furl) {

            boolean flag = true;
            boolean downloading =true;
            try{
                DownloadManager mManager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                Uri Download_Uri = Uri.parse(shiurimModel.getMp3());
                Log.e("adaddsa_Url",Download_Uri.toString());
                DownloadManager.Request mRqRequest = new DownloadManager.Request(Download_Uri);
                mRqRequest.setAllowedOverRoaming(false);
                mRqRequest.setTitle(shiurimModel.getTitle() + ".mp3");
                mRqRequest.allowScanningByMediaScanner();
                mRqRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                mRqRequest.setDescription(shiurimModel.getTopic());
                mRqRequest.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, shiurimModel.getTitle().replace("?","") + ".mp3");
                mRqRequest.setVisibleInDownloadsUi(true);
                DownloadManager.Query query = null;
                query = new DownloadManager.Query();
                Cursor c = null;
                if(query!=null) {
                    query.setFilterByStatus(DownloadManager.STATUS_FAILED|DownloadManager.STATUS_PAUSED|DownloadManager.STATUS_SUCCESSFUL|DownloadManager.STATUS_RUNNING|DownloadManager.STATUS_PENDING);
                } else {
                    return flag;
                }

                while (downloading) {
                    c = mManager.query(query);
                    if(c.moveToFirst()) {
                        Log.i ("FLAG","Downloading");
                        int status =c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));

                        if (status==DownloadManager.STATUS_SUCCESSFUL) {
                            Log.i ("FLAG","done");
                            flag=true;
                            break;
                        }
                        if (status==DownloadManager.STATUS_FAILED) {
                            Log.i ("FLAG","Fail");
                            flag=false;
                            break;
                        }
                    }
                }

                return flag;
            }catch (Exception e) {
                flag = false;
                return flag;
            }
        }

        protected void onProgressUpdate(Integer... progress) {
            pDialog.setProgress(progress[0]);
        }
        @Override
        protected void onPostExecute(Boolean downloaded) {
            try {
                if (downloaded) {
                } else {
                }
                if (type.equals(Constants.ACTIONS.getShiurim.name()) && shiurimAdapter != null) {
                    shiurimAdapter.setFileDownloading(-1);
                    shiurimAdapter.notifyDataSetChanged();
                }
            }catch (Exception e){}
        }
    }
}
