package aexyn.beis.medrash.fragments;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import aexyn.beis.medrash.pref.ReadPref;
import aexyn.beis.medrash.pref.SavePref;
import aexyn.beis.medrash.widget.Progressbar;
import io.realm.RealmObject;

public class BaseFragment extends Fragment {
    private OnFragmentInteractionListener mListener;
    private Progressbar progressbar;

    public BaseFragment() {
    }

    public Gson getGson() {
        return new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
    }

    public void enableMenu() {
        if (mListener != null) {
            mListener.enableMenu();
        }
    }

    public void disableMenu() {
        if (mListener != null) {
            mListener.disableMenu();
        }
    }
    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public SavePref getSavedPref() {
        return new SavePref(getActivity());
    }

    public ReadPref getreadPref() {
        return new ReadPref(getActivity());
    }

    public void showProgressBar() {
        if (progressbar == null)
            progressbar = Progressbar.show(getActivity());
    }

    public void hideProgressBar() {
        if (progressbar != null) {
            progressbar.dismiss();
            progressbar = null;
        }
    }

    public Context getContext() {
        return getActivity();
    }

    public void showError(TextView view, String message){
        view.setVisibility(View.GONE);
        view.setText(message);
    }

    public void hideError(View view){
        view.setVisibility(View.GONE);
    }

    public interface OnFragmentInteractionListener {
        void enableMenu();
        void disableMenu();
        void openDrawer();
        void updateCategoryList();
    }
    
}
