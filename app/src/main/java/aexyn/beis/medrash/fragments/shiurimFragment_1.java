package aexyn.beis.medrash.fragments;


import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import aexyn.beis.medrash.BuildConfig;
import aexyn.beis.medrash.R;
import aexyn.beis.medrash.activities.MainActivity;
import aexyn.beis.medrash.adapters.NotificationAdapter;
import aexyn.beis.medrash.adapters.Sherium_SearchAdapter;
import aexyn.beis.medrash.adapters.ShiurimAdapter_Frag1;
import aexyn.beis.medrash.asynctask.AsyncRequest;
import aexyn.beis.medrash.db.NotificationStorage;
import aexyn.beis.medrash.db.ShiurimStorage;
import aexyn.beis.medrash.model.Child;
import aexyn.beis.medrash.model.NotificationModel;
import aexyn.beis.medrash.model.ShiurimModel;
import aexyn.beis.medrash.pref.ReadPref;
import aexyn.beis.medrash.pref.SavePref;
import aexyn.beis.medrash.utils.Constants;
import aexyn.beis.medrash.utils.NetworkUtils;
import aexyn.beis.medrash.utils.PermissionUtility;
import io.realm.Realm;
import io.realm.RealmObject;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import static aexyn.beis.medrash.utils.Constants.ACTION;
import static aexyn.beis.medrash.utils.Constants.CATEGORY_ID;
import static aexyn.beis.medrash.utils.Constants.DATA;
import static aexyn.beis.medrash.utils.Constants.MESSAGE;
import static aexyn.beis.medrash.utils.Constants.NOTIFICATIONS;
import static aexyn.beis.medrash.utils.Constants.PAGE;
import static aexyn.beis.medrash.utils.Constants.SHIURIM;
import static aexyn.beis.medrash.utils.Constants.STATUS;
import static aexyn.beis.medrash.utils.Constants.TITLE;
import static aexyn.beis.medrash.utils.Constants.TOKEN;
import static aexyn.beis.medrash.utils.PermissionUtility.MY_PERMISSIONS_REQUEST_STORAGE_PERMISSION;
import static aexyn.beis.medrash.utils.Utils.showMessage;


public class shiurimFragment_1 extends BaseFragment{
    View rootView;
    private static final String ARG_TYPE = "type";
    private static final String mp3Id_Type = "mp3Id";
    private static final String Selected_title_Type = "Selected_title";
    private static final String TAG = "Search Sherium Activity";
    Sherium_SearchAdapter adapter;
    int i;
    ListView listView;
    ArrayList<Child> listCategories;
    ArrayList<ShiurimModel> ShiurimModel_listCategories;
    HashMap<Integer, List<Child>> listSubCategories;
    ShiurimAdapter_Frag1 shiurimAdapter;
    SwipeRefreshLayout swipeToRefresh;
    private boolean flag;
    NotificationAdapter notificationAdapter;
    private String type, mp3_ID, Selected_Title;
    String title = "", categoryID = "", position, Selected_Category = "", Selected_CategoryName;
    int pageNum = 0;
    TextView tvNoData, tvTitle;
    ShiurimModel downloadModel;
    SearchView searchView;
    ImageButton btnSearch;
    RelativeLayout layoutToolbar;
    int clickEvent = 0, backmenu_Inc = 0;
    String BackStack;
    String Search_CatID, webview_shiurim_check_Status = "", ServerSearchIFEnable = "";
    ImageView backMenu;
    ImageView most_recent_shiurim_bt;


    public static shiurimFragment_1 newInstance(String type, String mp3Id, String Selected_title/*,String MostRecent_shiurim_clicked*/) {
        shiurimFragment_1 fragment = new shiurimFragment_1();
        Bundle args = new Bundle();
        args.putString(ARG_TYPE, type);
        args.putString(mp3Id_Type, mp3Id);
        args.putString(Selected_title_Type, Selected_title);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            type = getArguments().getString(ARG_TYPE);
            mp3_ID = getArguments().getString(mp3Id_Type);
            Selected_Title = getArguments().getString(Selected_title_Type);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        rootView = inflater.inflate(R.layout.activity_search_sherium, container, false);
        listView = (ListView) rootView.findViewById(R.id.sheriumlist);
        swipeToRefresh = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeToRefresh);
        layoutToolbar = (RelativeLayout) rootView.findViewById(R.id.layoutToolbar);
        tvNoData = (TextView) rootView.findViewById(R.id.tvNoData);
        searchView = (SearchView) rootView.findViewById(R.id.searchView);


        most_recent_shiurim_bt = (ImageView) rootView.findViewById(R.id.most_recent_shiurim_bt);
        most_recent_shiurim_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.shiurimClicked_weview("webview_shiurim");
                new SavePref(getActivity()).Sherium_For_webiView("webview_shiurim_Status", "webview_shiurim");
            }
        });

        backMenu = (ImageView) rootView.findViewById(R.id.backMenu);
        if (clickEvent != 0) {
            backMenu.setVisibility(View.VISIBLE);
        } else if (clickEvent == 0 && webview_shiurim_check_Status.equals("true")) {
            backMenu.setVisibility(View.VISIBLE);

        } else if (clickEvent == 0 && ServerSearchIFEnable.equals("true")) {
            backMenu.setVisibility(View.VISIBLE);

        } else {
            backMenu.setVisibility(View.GONE);
        }

        tvNoData.setVisibility(View.GONE);
        if (NetworkUtils.isNetworkConnected(getActivity())) {
            getCategories();
            if (type.equals(Constants.ACTIONS.getNotifications.name())) {

            } else {
                setScrollListener();
            }
        } else {
            if (type.equals(Constants.ACTIONS.getNotifications.name())) {
                if (getreadPref().keyExixst(NOTIFICATIONS)) {
                    setList();
                } else {
                    getCategories();
                }
            } else {
                if (getreadPref().keyExixst(SHIURIM)) {
                    setList();
                } else {
                    getCategories();
                }
                setScrollListener();
            }
        }

        backMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backpressed();
            }
        });
        btnSearch = (ImageButton) rootView.findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setVisibility(View.VISIBLE);
                layoutToolbar.setVisibility(View.GONE);
                searchView.setIconified(false);
                searchView.setOnCloseListener(new SearchView.OnCloseListener() {
                    @Override
                    public boolean onClose() {
                        layoutToolbar.setVisibility(View.VISIBLE);
                        searchView.setVisibility(View.GONE);
                        if (!title.isEmpty()) {
                            title = "";
                            ServerSearchIFEnable = "true";
                            setPageNum(1);
                            loadData(title);
                        }
                        return false;
                    }
                });
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        title = query;
                        ServerSearchIFEnable = "";
                        setPageNum(1);
                        loadData(title);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        return false;
                    }
                });
            }
        });

        tvTitle = (TextView) rootView.findViewById(R.id.tvTitle);
        tvTitle.setText(Selected_Title);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TextView _Search_Category_Name = (TextView) view.findViewById(R.id.Search_Category_Name);
                Selected_CategoryName = _Search_Category_Name.getText().toString();

                TextView Search_Category_id = (TextView) view.findViewById(R.id.id);
                Selected_Category = Search_Category_id.getText().toString();
                BackStack = new ReadPref(getActivity()).getCategory_ForSherium("Maintain_backStack");

                if (BackStack.equals("") || BackStack == null) {
                    new SavePref(getActivity()).saveCategory_ForSherium("Maintain_backStack", String.valueOf(Selected_Category));
                } else {
                    new SavePref(getActivity()).saveCategory_ForSherium("Maintain_backStack", String.valueOf(BackStack + "," + Selected_Category));
                }
                clickEvent++;
                setPageNum(1);
                setCategoryID(String.valueOf(Selected_Category));
                Search_CatID = (String.valueOf(Selected_Category));
                getCategories();
                backmenu_Inc++;
            }


        });


        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageNum = 1;
                getCategories();
            }
        });

        new SavePref(getActivity()).remove("Maintain_backStack");

        return rootView;
    }


    public void most_recent_shiurim_bt_VISIBLE() {

        most_recent_shiurim_bt.setVisibility(View.VISIBLE);

    }

    public void most_recent_shiurim_bt_HIDE() {

        most_recent_shiurim_bt.setVisibility(View.GONE);

    }

    public void backpressed() {
        try {

            if (webview_shiurim_check_Status.equals("true")) {
                MainActivity.homeClicked();
            } else if (ServerSearchIFEnable.equals("true")) {
                getCategories();
            } else {
                tvNoData.setVisibility(View.GONE);
                backmenu_Inc--;

                String BackStack = new ReadPref(getActivity()).getCategory_ForSherium("Maintain_backStack");
                String[] Clicked_Sherium_BackStack = BackStack.split(",");
                List<String> list = new ArrayList<String>();
                for (i = 0; i < Clicked_Sherium_BackStack.length; i++) {

                    list.add(Clicked_Sherium_BackStack[i]);
                    list.remove("");

                }
                if (!BackStack.equals("") && list.size() != 0 && Clicked_Sherium_BackStack.length != 0) {
                    Clicked_Sherium_BackStack[clickEvent - 1] = null;

                    new SavePref(getActivity()).remove("Maintain_backStack");

                    if (list.size() != 0) {

                        int poss = 0;
                        String ConcatenateVal = "";


                        for (int i = 0; i < list.size(); i++) {

                            String arrayName = Clicked_Sherium_BackStack[i];

                            if (arrayName != null && i == list.size()) {

                                ConcatenateVal = ConcatenateVal.concat(list.get(poss));

                            } else if (arrayName != null && i != list.size()) {
                                ConcatenateVal = ConcatenateVal.concat(list.get(poss) + ",");
                            }
                            poss++;
                        }

                        new SavePref(getActivity()).saveCategory_ForSherium("Maintain_backStack", ConcatenateVal);

                        String[] Clicked_Sherium_FinalBackStack = ConcatenateVal.split(",");
                        int len = Clicked_Sherium_FinalBackStack.length;
                        setCategoryID(Clicked_Sherium_FinalBackStack[len - 1]);
                        Search_CatID = Clicked_Sherium_FinalBackStack[len - 1];

                        getCategories();
                        clickEvent--;
                    }


                } else {
                    getActivity().finish();
                }

            }
        } catch (Exception e) {


        }

    }
    public void getCategories() {
        listView.setVisibility(View.INVISIBLE);
        tvNoData.setVisibility(View.GONE);
        listCategories = new ArrayList<>();
        listSubCategories = new HashMap<>();

        if (shiurimAdapter != null) {
            shiurimAdapter.clear();
            shiurimAdapter = null;
        }
        if (NetworkUtils.isNetworkConnected(getActivity())) {


            showProgressBar();

            disableMenu();
            swipeToRefresh.post(new Runnable() {
                @Override
                public void run() {
                    swipeToRefresh.setRefreshing(true);
                }
            });
            RequestBody formBody = new FormBody.Builder()
                    .add(TOKEN, BuildConfig.token)
                    .add(ACTION, Constants.ACTIONS.getcategory.name())
                    .add(CATEGORY_ID, categoryID)
                    .build();
            new AsyncRequest(BuildConfig.apiDomain, formBody, false, new AsyncRequest.OnTaskCompleted() {
                @Override
                public void onTaskCompleted(AsyncRequest asyncRequest, String result) {
                    enableMenu();
                    if (asyncRequest != null) {
                        try {
                            result = asyncRequest.get();

                            Log.e(TAG, "" + result);
                            JSONObject jsonObject1 = new JSONObject(result);
                            if (jsonObject1.getString(STATUS).equals(Constants.STATUSES.Success.name())) {
                                JSONArray jsonArray = jsonObject1.getJSONArray(DATA);


                                if (jsonArray.length() == 0) {
                                    loadData("");
                                } else {
                                    setScrollListener();
                                    listCategories.clear();
                                    listSubCategories.clear();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        Child category = getGson().fromJson(jsonArray.getString(i).toString(), Child.class);
                                        listCategories.add(category);
                                        try {
                                            if (category.getChild().length > 0)
                                                listSubCategories.put(category.getId(), new ArrayList<>(Arrays.asList(category.getChild())));
                                        } catch (NullPointerException e) {

                                        } catch (Exception e) {
                                        }
                                    }

                                    if (backmenu_Inc != 0) {
                                        backMenu.setVisibility(View.VISIBLE);
                                    } else {
                                        backMenu.setVisibility(View.INVISIBLE);
                                    }
                                    enableMenu();
                                    adapter = new Sherium_SearchAdapter(getActivity(), listCategories, listSubCategories, mp3_ID);
                                    listView.setAdapter(adapter);
                                    listView.setVisibility(View.VISIBLE);
                                }

                            } else {
                                showMessage(getActivity(), jsonObject1.getString(MESSAGE), false);
                                enableMenu();
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            enableMenu();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                            enableMenu();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            enableMenu();
                        }
                    }
                    swipeToRefresh.setRefreshing(false);
                    hideProgressBar();
                }

            }).execute();
        } else {

            showMessage(getActivity(), getString(R.string.no_network), false);
            swipeToRefresh.post(new Runnable() {
                @Override
                public void run() {
                    swipeToRefresh.setRefreshing(false);
                }
            });
            enableMenu();
            return;
        }
    }

    public Gson getGson() {
        return new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
    }

    public void setScrollListener() {
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                if (scrollState == 2)
                    flag = true;
                Log.i("Scroll State", "" + scrollState);
            }

            @Override
            public void onScroll(AbsListView lw, final int firstVisibleItem,
                                 final int visibleItemCount, final int totalItemCount) {
                if ((visibleItemCount == (totalItemCount - firstVisibleItem))
                        && flag) {
                    flag = false;
                    Log.i("Scroll", "Ended");
                    pageNum++;
                }
                boolean enable = false;
                if (listView != null && listView.getChildCount() > 0) {
                    boolean firstItemVisible = listView.getFirstVisiblePosition() == 0;
                    boolean topOfFirstItemVisible = listView.getChildAt(0).getTop() == 0;
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                swipeToRefresh.setEnabled(enable);

            }
        });

    }


    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public void setwebview_shiurim_check(String shiurim_check_Status) {
        this.webview_shiurim_check_Status = shiurim_check_Status;
    }
    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public void loadData(String title) {
        if (adapter != null) {
            adapter.clear();
            adapter = null;
        }
        if (!NetworkUtils.isNetworkConnected(getActivity())) {
            showMessage(getActivity(), getString(R.string.no_network), false);
            swipeToRefresh.post(new Runnable() {
                @Override
                public void run() {
                    swipeToRefresh.setRefreshing(false);
                }
            });
            enableMenu();
            return;
        }
        this.title = title;
        disableMenu();
        swipeToRefresh.post(new Runnable() {
            @Override
            public void run() {
                swipeToRefresh.setRefreshing(true);
            }
        });

        RequestBody formBody;
        showProgressBar();

        if (type.equals(Constants.ACTIONS.getNotifications.name())) {
            formBody = new FormBody.Builder()
                    .add(TOKEN, BuildConfig.token)
                    .add(ACTION, type)
                    .build();
        } else {
            if (categoryID.isEmpty()) {
                if (title.isEmpty()) {
                    formBody = new FormBody.Builder()
                            .add(TOKEN, BuildConfig.token)
                            .add(ACTION, type)
                            .add("count", "5")
                            .add(PAGE, String.valueOf(pageNum))
                            .build();
                } else {
                    formBody = new FormBody.Builder()
                            .add(TOKEN, BuildConfig.token)
                            .add(ACTION, type)
                            .add(TITLE, title)
                            .add("count", "5")
                            .add(PAGE, String.valueOf(pageNum))
                            .build();
                }
            } else {
                if (title.isEmpty()) {
                    formBody = new FormBody.Builder()
                            .add(TOKEN, BuildConfig.token)
                            .add(ACTION, type)
                            .add(PAGE, String.valueOf(pageNum))
                            .add(CATEGORY_ID, categoryID)
                            .build();
                } else {
                    formBody = new FormBody.Builder()
                            .add(TOKEN, BuildConfig.token)
                            .add(ACTION, type)
                            .add(TITLE, title)
                            .add(PAGE, String.valueOf(pageNum))
                            .add(CATEGORY_ID, categoryID)
                            .build();
                }
            }
        }
        new AsyncRequest(BuildConfig.apiDomain, formBody, false, new AsyncRequest.OnTaskCompleted() {
            @Override
            public void onTaskCompleted(AsyncRequest asyncRequest, String result) {
                if (asyncRequest != null) {
                    try {
                        result = asyncRequest.get();
                        listView.setVisibility(View.INVISIBLE);
                        Log.e(TAG, "" + result);
                        JSONObject jsonObject1 = new JSONObject(result);
                        if (jsonObject1.getString(STATUS).equals(Constants.STATUSES.Success.name())) {
                            if (shiurimAdapter != null) {
                                if (pageNum == 1) {
                                    shiurimAdapter.clear();
                                    ShiurimStorage.removeAll();
                                }
                            }
                            if (notificationAdapter != null) {
                                notificationAdapter.clear();
                                NotificationStorage.removeAll();
                            }
                            JSONArray jsonArray = jsonObject1.getJSONArray(DATA);
                            if (jsonArray.length() == 0) {
                                listView.setOnScrollListener(new AbsListView.OnScrollListener() {
                                    @Override
                                    public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                                    }

                                    @Override
                                    public void onScroll(AbsListView lw, final int firstVisibleItem,
                                                         final int visibleItemCount, final int totalItemCount) {
                                        boolean enable = false;
                                        if (listView != null && listView.getChildCount() > 0) {
                                            boolean firstItemVisible = listView.getFirstVisiblePosition() == 0;
                                            boolean topOfFirstItemVisible = listView.getChildAt(0).getTop() == 0;
                                            enable = firstItemVisible && topOfFirstItemVisible;
                                        }
                                        swipeToRefresh.setEnabled(enable);
                                    }
                                });
                            } else {
                                setScrollListener();
                            }
                            ShiurimModel_listCategories = new ArrayList<>();
                            ShiurimStorage.removeAll();
                            for (int i = 0; i < jsonArray.length(); i++) {

                                if (type.equals(Constants.ACTIONS.getNotifications.name())) {
                                    getSavedPref().saveBoolean(NOTIFICATIONS, true);

                                    NotificationModel notificationModel = getGson().fromJson(jsonArray.getString(i), NotificationModel.class);
                                    NotificationStorage.save(notificationModel);
                                } else {
                                    getSavedPref().saveBoolean(SHIURIM, true);


                                    JSONObject tagJsonObject = jsonArray.getJSONObject(i);
                                    int Duration = tagJsonObject.getInt("duration");

                                    String mp3 = tagJsonObject.getString("mp3").toLowerCase();
                                    if(!mp3.contains("pdf")) {
                                        if (Duration != 0) {
                                            ShiurimModel shiurimModel = getGson().fromJson(jsonArray.getString(i), ShiurimModel.class);
                                            ShiurimStorage.save(shiurimModel);

                                            ShiurimModel_listCategories.add(shiurimModel);
                                        }
                                    }else{
                                        ShiurimModel shiurimModel = getGson().fromJson(jsonArray.getString(i), ShiurimModel.class);
                                        ShiurimStorage.save(shiurimModel);

                                        ShiurimModel_listCategories.add(shiurimModel);
                                    }
                                }
                            }

                            if (jsonArray.length() != 0) {
                                setList();
                            } else if (jsonArray.length() == 0) {

                                hideProgressBar();
                                enableMenu();
                                tvNoData.setVisibility(View.VISIBLE);

                            } else {
                                tvNoData.setVisibility(View.GONE);
                            }


                        } else {
                            enableMenu();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        enableMenu();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                        enableMenu();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        enableMenu();
                    }
                }
                swipeToRefresh.setRefreshing(false);
            }

        }).execute();
    }

    public void setList() {
        try {
            enableMenu();
            if (type.equals(Constants.ACTIONS.getNotifications.name())) {

                if (backmenu_Inc != 0) {
                    backMenu.setVisibility(View.VISIBLE);

                } else if (backmenu_Inc == 0 && webview_shiurim_check_Status.equals("true")) {
                    backMenu.setVisibility(View.VISIBLE);
                } else if (backmenu_Inc == 0 && ServerSearchIFEnable.equals("true")) {
                    backMenu.setVisibility(View.VISIBLE);

                } else {
                    backMenu.setVisibility(View.GONE);
                }
                notificationAdapter = new NotificationAdapter(getActivity(), new ArrayList<NotificationModel>());
                listView.setAdapter(notificationAdapter);
                List<NotificationModel> list = NotificationStorage.getAll(Realm.getDefaultInstance());
                notificationAdapter.addAll(list);
                if (notificationAdapter.getCount() == 0) {
                    tvNoData.setVisibility(View.VISIBLE);
                } else {
                    tvNoData.setVisibility(View.GONE);
                }
                listView.setOnScrollListener(null);
            } else {
                if (backmenu_Inc != 0) {
                    backMenu.setVisibility(View.VISIBLE);
                } else if (backmenu_Inc == 0 && webview_shiurim_check_Status.equals("true")) {
                    backMenu.setVisibility(View.VISIBLE);

                } else if (backmenu_Inc == 0 && ServerSearchIFEnable.equals("true")) {
                    backMenu.setVisibility(View.VISIBLE);

                } else {
                    backMenu.setVisibility(View.GONE);
                }
                shiurimAdapter = new ShiurimAdapter_Frag1(getActivity(), new ArrayList<ShiurimModel>(), ShiurimModel_listCategories, this);
                listView.setAdapter(shiurimAdapter);
                listView.setVisibility(View.VISIBLE);
                for (int i = 0; i < ShiurimModel_listCategories.size(); i++) {

                    String mpsggetID = String.valueOf(ShiurimModel_listCategories.get(i).getId());
                    if (mpsggetID.equals(mp3_ID)) {
                        position = String.valueOf(i);
                        final SharedPreferences prefs = getActivity().getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("position", position);
                        editor.apply();
                        mp3_ID = "";
                        break;
                    }
                }
                hideProgressBar();
                if (shiurimAdapter.getCount() == 0) {
                    tvNoData.setVisibility(View.VISIBLE);
                } else {
                    tvNoData.setVisibility(View.GONE);
                }
            }
        } catch (NullPointerException e) {

        } catch (Exception e) {
        }
    }
    public void downloadFile(ShiurimModel shiurimModel) {
        if (!NetworkUtils.isNetworkConnected(getActivity())) {
            showMessage(getActivity(), getString(R.string.no_network), false);
            return;
        }
        downloadModel = shiurimModel;
        if (PermissionUtility.checkPermission(getActivity()))
            new DownloadFileFromURL(shiurimModel).execute();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_STORAGE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    downloadFile(downloadModel);
                }
                break;
        }
    }

    private class DownloadFileFromURL extends AsyncTask<String, Integer, Boolean> {

        private ShiurimModel shiurimModel;
        private ProgressDialog pDialog;

        DownloadFileFromURL(ShiurimModel shiurimModel) {
            this.shiurimModel = shiurimModel;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... furl) {

            boolean flag = true;
            boolean downloading = true;
            try {
                DownloadManager mManager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                Uri Download_Uri = Uri.parse(shiurimModel.getMp3());
                Log.e("adaddsa_Url",Download_Uri.toString());
                DownloadManager.Request mRqRequest = new DownloadManager.Request(Download_Uri);
                mRqRequest.setAllowedOverRoaming(false);
                if (shiurimModel.getMp3().contains(".mp3")) {
                    mRqRequest.setTitle(shiurimModel.getTitle() + ".mp3");
                } else {

                    mRqRequest.setTitle(shiurimModel.getTitle() + ".pdf");
                }
                mRqRequest.allowScanningByMediaScanner();
                mRqRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                mRqRequest.setDescription(shiurimModel.getTopic());
                if (shiurimModel.getMp3().contains(".mp3")) {
                    mRqRequest.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, shiurimModel.getTitle().replace("?", "") + ".mp3");
                } else {
                    mRqRequest.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, shiurimModel.getTitle().replace("?", "") + ".pdf");
                }
                mRqRequest.setVisibleInDownloadsUi(true);
                DownloadManager.Query query = null;
                query = new DownloadManager.Query();
                Cursor c = null;
                if (query != null) {
                    query.setFilterByStatus(DownloadManager.STATUS_FAILED | DownloadManager.STATUS_PAUSED | DownloadManager.STATUS_SUCCESSFUL | DownloadManager.STATUS_RUNNING | DownloadManager.STATUS_PENDING);
                } else {
                    return flag;
                }

                while (downloading) {
                    c = mManager.query(query);
                    if (c.moveToFirst()) {
                        Log.i("FLAG", "Downloading");
                        int status = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));

                        if (status == DownloadManager.STATUS_SUCCESSFUL) {
                            Log.i("FLAG", "done");
                            flag = true;
                            break;
                        }
                        if (status == DownloadManager.STATUS_FAILED) {
                            Log.i("FLAG", "Fail");
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            } catch (Exception e) {
                flag = false;
                return flag;
            }
        }

        protected void onProgressUpdate(Integer... progress) {
            pDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(Boolean downloaded) {
            try {
                if (downloaded) {
                } else {
                }
                if (type.equals(Constants.ACTIONS.getShiurim.name()) && shiurimAdapter != null) {
                    shiurimAdapter.setFileDownloading(-1);
                    shiurimAdapter.notifyDataSetChanged();
                }
            } catch (Exception e) {}
        }
    }
}
