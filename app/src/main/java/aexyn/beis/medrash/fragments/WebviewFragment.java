package aexyn.beis.medrash.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.MailTo;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.concurrent.ExecutionException;
import aexyn.beis.medrash.BuildConfig;
import aexyn.beis.medrash.R;
import aexyn.beis.medrash.activities.Announcement_webview;
import aexyn.beis.medrash.activities.MainActivity;
import aexyn.beis.medrash.asynctask.AsyncRequest;
import aexyn.beis.medrash.pref.SavePref;
import aexyn.beis.medrash.utils.Constants;
import aexyn.beis.medrash.utils.NetworkUtils;
import im.delight.android.webview.AdvancedWebView;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import static aexyn.beis.medrash.utils.Constants.ABOUT;
import static aexyn.beis.medrash.utils.Constants.ACTION;
import static aexyn.beis.medrash.utils.Constants.ANNOUNCEMENTS;
import static aexyn.beis.medrash.utils.Constants.DATA;
import static aexyn.beis.medrash.utils.Constants.HOME;
import static aexyn.beis.medrash.utils.Constants.MESSAGE;
import static aexyn.beis.medrash.utils.Constants.STATUS;
import static aexyn.beis.medrash.utils.Constants.TOKEN;
import static aexyn.beis.medrash.utils.Constants.WEEKLY_ANNOUNCEMENT;
import static aexyn.beis.medrash.utils.Constants.ZEMANIM;
import static aexyn.beis.medrash.utils.Utils.showMessage;

public class WebviewFragment extends BaseFragment implements AdvancedWebView.Listener, View.OnClickListener {

    private static final String ARG_TYPE = "type";
    public static String TAG = "WebviewFragment";
    View rootView;
    AdvancedWebView mWebView;
    TextView tvRefresh;
    SwipeRefreshLayout swipeToRefresh;
    private String type;
    Button valid,refuse;

    public WebviewFragment() {
    }

    public static WebviewFragment newInstance(String type) {
        WebviewFragment fragment = new WebviewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getString(ARG_TYPE);
        }
    }

    @SuppressLint("JavascriptInterface")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_web_view, container, false);
        mWebView = (AdvancedWebView) rootView.findViewById(R.id.webview);
        tvRefresh = (TextView) rootView.findViewById(R.id.tvRefresh);
        swipeToRefresh = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeToRefresh);
        tvRefresh.setOnClickListener(this);

        mWebView.setListener(getActivity(), this);
        mWebView.getSettings().setAllowContentAccess(true);
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.getSettings().setJavaScriptEnabled(true);

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("mailto:")) {
                    MailTo mt = MailTo.parse(url);
                    Intent i = newEmailIntent(getActivity(), mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc());
                    if (i != null)
                        startActivity(i);
                }else if(url.startsWith("http://recentshiurim.com/")){

                    MainActivity.shiurimClicked_weview("webview_shiurim");
                    new SavePref(getActivity()).Sherium_For_webiView("webview_shiurim_Status", "webview_shiurim");

                }else if(url.startsWith("http://weeklydata.com/")){
                    Intent inn=new Intent(getActivity(), Announcement_webview.class);
                    inn.putExtra("type","weekly");
                    startActivity(inn);
                }else if(url.startsWith("http://shiurimAnnouncement.com/")){
                    Intent inn=new Intent(getActivity(), Announcement_webview.class);
                    inn.putExtra("type","announcement");
                    startActivity(inn);
                } else {

                    view.loadUrl(url);
                }
                return true;
            }
        });

        loadData();



        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getSavedPref().remove(type);
                loadData();
            }
        });
        Button valid = new Button(getActivity());
        valid.setOnClickListener(this);
        Button refuse = new Button(getActivity());
        refuse.setOnClickListener(this);

        return rootView;
    }
    public void loadData(){
        if(NetworkUtils.isNetworkConnected(getActivity())){
            load();
        }else {
            if (!getreadPref().keyExixst(type)) {
                load();
            } else {
                loadDataInWebView();
            }
        }
    }

    public void load(){
        if (!NetworkUtils.isNetworkConnected(getActivity())) {
            showMessage(getActivity(), getString(R.string.no_network), false);
            swipeToRefresh.post(new Runnable() {
                @Override
                public void run() {
                    swipeToRefresh.setRefreshing(false);
                }
            });
            enableMenu();
            return;
        }
        swipeToRefresh.post(new Runnable() {
            @Override
            public void run() {
                swipeToRefresh.setRefreshing(true);
            }
        });
        disableMenu();
        RequestBody formBody = new FormBody.Builder()
                .add(TOKEN, BuildConfig.token)
                .add(ACTION, Constants.ACTIONS.coreData.name())
                .build();
        new AsyncRequest(BuildConfig.apiDomain, formBody, false, new AsyncRequest.OnTaskCompleted() {
            @Override
            public void onTaskCompleted(AsyncRequest asyncRequest, String result) {
                if (asyncRequest != null) {
                    try {
                        hideError(tvRefresh);
                        result = asyncRequest.get();
                        Log.e(TAG, "" + result);
                        JSONObject jsonObject1 = new JSONObject(result);
                        if (jsonObject1.getString(STATUS).equals(Constants.STATUSES.Success.name())) {
                            getSavedPref().saveString(HOME, jsonObject1.getJSONObject(DATA).getString(HOME));
                            getSavedPref().saveString(ABOUT, jsonObject1.getJSONObject(DATA).getString(ABOUT));
                            getSavedPref().saveString(ZEMANIM, jsonObject1.getJSONObject(DATA).getString(ZEMANIM));
                            getSavedPref().saveString(ANNOUNCEMENTS, jsonObject1.getJSONObject(DATA).getString(ANNOUNCEMENTS));
                            getSavedPref().saveString(WEEKLY_ANNOUNCEMENT, jsonObject1.getJSONObject(DATA).getString(WEEKLY_ANNOUNCEMENT));
                            loadDataInWebView();
                        } else {
                            showError(tvRefresh, jsonObject1.getString(MESSAGE));
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        showError(tvRefresh, getString(R.string.something_went_wrong));
                        enableMenu();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                        showError(tvRefresh, getString(R.string.something_went_wrong));
                        enableMenu();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        showError(tvRefresh, getString(R.string.something_went_wrong));
                        enableMenu();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
                swipeToRefresh.post(new Runnable() {
                    @Override
                    public void run() {
                        swipeToRefresh.setRefreshing(false);
                    }
                });
            }

        }).execute();
    }

    public void loadDataInWebView(){
        mWebView.loadData(getreadPref().getStringValue(type), "text/html", "UTF-8");
    }

    private Intent newEmailIntent(Context context, String address, String subject, String body, String cc) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{address});
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_CC, cc);
        intent.setType("message/rfc822");
        return intent;
    }

    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();
        mWebView.onResume();
    }
    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        mWebView.onPause();
        super.onPause();
    }
    @Override
    public void onDestroy() {
        mWebView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);
    }
    @Override
    public void onPageStarted(String s, Bitmap bitmap) {
        showProgressBar();
    }
    @Override
    public void onPageFinished(String s) {
        hideProgressBar();
        enableMenu();
    }
    @Override
    public void onPageError(int i, String s, String s1) {
        enableMenu();
    }
    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) { }
    @Override
    public void onExternalPageRequest(String s) {
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tvRefresh:
                getSavedPref().remove(type);
                loadData();
                break;
        }
        if (view.equals(valid)) {
            Toast.makeText(getActivity(),"Valid",Toast.LENGTH_LONG).show();
        } else if (view.equals(refuse)) {
            Toast.makeText(getActivity(),"Refuse",Toast.LENGTH_LONG).show();
        }
    }
}
