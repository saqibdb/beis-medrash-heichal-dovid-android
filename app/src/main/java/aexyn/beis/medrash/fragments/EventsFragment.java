package aexyn.beis.medrash.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import aexyn.beis.medrash.BuildConfig;
import aexyn.beis.medrash.R;
import aexyn.beis.medrash.activities.Event_Dialog;
import aexyn.beis.medrash.asynctask.AsyncRequest;
import aexyn.beis.medrash.db.EventStorage;
import aexyn.beis.medrash.decorators.EventDecorator;
import aexyn.beis.medrash.model.EventModel;
import aexyn.beis.medrash.utils.Constants;
import aexyn.beis.medrash.utils.NetworkUtils;
import im.delight.android.webview.AdvancedWebView;
import io.realm.Realm;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import static aexyn.beis.medrash.utils.Constants.ACTION;
import static aexyn.beis.medrash.utils.Constants.DATA;
import static aexyn.beis.medrash.utils.Constants.EVENTS;
import static aexyn.beis.medrash.utils.Constants.MESSAGE;
import static aexyn.beis.medrash.utils.Constants.STATUS;
import static aexyn.beis.medrash.utils.Constants.TOKEN;
import static aexyn.beis.medrash.utils.Utils.showMessage;

public class EventsFragment extends BaseFragment implements View.OnClickListener, OnDateSelectedListener, AdvancedWebView.Listener {

    public static String TAG = "EventsFragment";
    static MaterialCalendarView calendarView;
    TextView tvRefresh, devingtab_Text;
    View rootView;
    Calendar currentCalendar;
    int currentWeekNum;
    ImageAdapter imageAdapter;
    List<EventModel> eventModelList;
    static List<EventModel> eventModelListFinal;
    static String NotificationOther;
    static String EventID="";
    String devingtab,notification_status="";
    AdvancedWebView devingtabwebview;


    public EventsFragment() {
    }

    public static EventsFragment newInstance(String notificationOther) {
        EventsFragment fragment = new EventsFragment();

        NotificationOther = notificationOther;
        if (NotificationOther != null && !NotificationOther.equals("[]")) {

            String[] splited = NotificationOther.split(",");
            String splited0 = splited[0];
            String[] splited1 = splited0.split(":");

            EventID = splited1[1];
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }


    }

    @SuppressLint("JavascriptInterface")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_event, container, false);
        devingtabwebview = (AdvancedWebView) rootView.findViewById(R.id.devingtabwebview);
        calendarView = (MaterialCalendarView) rootView.findViewById(R.id.calendarView);
        tvRefresh = (TextView) rootView.findViewById(R.id.tvRefresh);
        devingtab_Text = (TextView) rootView.findViewById(R.id.devingtab);
        tvRefresh.setOnClickListener(this);
        devingtabwebview.setListener(getActivity(), this);
        devingtabwebview.getSettings().setAllowContentAccess(true);
        devingtabwebview.getSettings().setAllowFileAccess(true);
        devingtabwebview.getSettings().setJavaScriptEnabled(true);
        devingtabwebview.getSettings().setAppCacheEnabled(true);
        devingtabwebview.getSettings().setJavaScriptEnabled(true);

        devingtabwebview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        currentCalendar = Calendar.getInstance();
        currentWeekNum = currentCalendar.get(Calendar.WEEK_OF_YEAR);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (NetworkUtils.isNetworkConnected(getActivity())) {
                    loadData();
                } else {
                    if (getreadPref().keyExixst(EVENTS)) {
                        eventModelList = EventStorage.getAll(Realm.getDefaultInstance());
                        showEvents();
                    } else {
                        loadData();
                    }
                }
            }
        }, 1000);
        calendarView.setOnDateChangedListener(this);
        calendarView
                .state()
                .edit()
                .setCalendarDisplayMode(CalendarMode.WEEKS)
                .commit();

        return rootView;
    }

    public void loadData() {
        if (!NetworkUtils.isNetworkConnected(getActivity())) {
            showMessage(getActivity(), getString(R.string.no_network), false);
            enableMenu();
            return;
        }
        disableMenu();
        showProgressBar();
        RequestBody formBody = new FormBody.Builder()
                .add(TOKEN, BuildConfig.token)
                .add(ACTION, Constants.ACTIONS.getEvents.name())
                .build();
        new AsyncRequest(BuildConfig.apiDomain, formBody, false, new AsyncRequest.OnTaskCompleted() {
            @Override
            public void onTaskCompleted(AsyncRequest asyncRequest, String result) {
                enableMenu();
                if (asyncRequest != null) {
                    try {
                        eventModelList = new ArrayList<>();
                        eventModelListFinal = new ArrayList<>();
                        result = asyncRequest.get();
                        Log.e(TAG, "" + result);
                        JSONObject jsonObject1 = new JSONObject(result);

                        devingtab = jsonObject1.getString("devingtab");

                        if (!devingtab.isEmpty() && !devingtab.equals("")) {
                            devingtabwebview.setVisibility(View.VISIBLE);
                            loadDataInWebView();
                        } else {
                            devingtabwebview.setVisibility(View.GONE);
                        }
                        if (jsonObject1.getString(STATUS).equals(Constants.STATUSES.Success.name())) {
                            EventStorage.removeAll();
                            calendarView.removeDecorators();
                            hideError(tvRefresh);
                            calendarView.setVisibility(View.VISIBLE);
                            getSavedPref().saveBoolean(EVENTS, true);

                            for (int i = 0; i < jsonObject1.getJSONArray(DATA).length(); i++) {
                                EventModel eventModel = getGson().fromJson(jsonObject1.getJSONArray(DATA).getString(i), EventModel.class);
                                String str_date = eventModel.getDate();
                                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                                Date date = (Date) formatter.parse(str_date);
                                CalendarDay day = CalendarDay.from(date);
                                Calendar cl = day.getCalendar();
                                long timeStamp = cl.getTimeInMillis();
                                eventModel.setTimeStamp(timeStamp);
                                int weekNum = day.getCalendar().get(Calendar.WEEK_OF_YEAR);
                                int diff = weekNum - currentWeekNum;
                                eventModel.setWeekDiff(diff);
                                eventModelList.add(eventModel);
                                EventStorage.save(eventModel);
                            }

                            for (int i = eventModelList.size() - 1; i >= 0; i--) {
                                eventModelListFinal.add(eventModelList.get(i));
                            }

                            calendarView.clearSelection();
                            if(notification_status.equals("Notification_Arrived")){
                                ShowcurrentNotificationDateEvent();
                            }else {
                                showEvents();
                            }

                        } else {
                            showError(tvRefresh, jsonObject1.getString(MESSAGE));
                            calendarView.setVisibility(View.GONE);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        showError(tvRefresh, getString(R.string.something_went_wrong));
                        calendarView.setVisibility(View.GONE);
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                        showError(tvRefresh, getString(R.string.something_went_wrong));
                        calendarView.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        showError(tvRefresh, getString(R.string.something_went_wrong));
                        calendarView.setVisibility(View.GONE);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {

                    }
                }
                hideProgressBar();
            }

        }).execute();
    }


    public void loadDataInWebView() {
        devingtabwebview.loadData(devingtab, "text/html", "UTF-8");
    }

    void showEvents() {

        try {
            ArrayList<CalendarDay> dates = new ArrayList<>();
            ArrayList<String> events = new ArrayList<>();
            for (EventModel eventModel : eventModelListFinal) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(eventModel.getTimeStamp());
                CalendarDay calendarDay = CalendarDay.from(calendar);
                dates.add(calendarDay);
                String event = String.valueOf(eventModel.getId());
                events.add(event);
            }
            Calendar newCalendar = Calendar.getInstance();
            newCalendar.set(Calendar.HOUR, 0);
            newCalendar.set(Calendar.MINUTE, 0);
            newCalendar.set(Calendar.MILLISECOND, 0);
            Calendar currentCalendar = Calendar.getInstance();
            CalendarDay calendarDay = calendarView.getCurrentDate();
            currentCalendar.set(Calendar.DAY_OF_MONTH, calendarDay.getDay());
            currentCalendar.set(Calendar.MONTH, calendarDay.getMonth());
            currentCalendar.set(Calendar.YEAR, calendarDay.getYear());

            if (NotificationOther == null) {
                int diff = newCalendar.get(Calendar.WEEK_OF_YEAR) - currentCalendar.get(Calendar.WEEK_OF_YEAR);
                if (diff > 0) {
                    for (int ik = 0; ik < diff; ik++) {
                        calendarView.goToNext();
                    }
                } else {
                    for (int ik = 0; ik < Math.abs(diff); ik++) {
                        calendarView.goToPrevious();
                    }
                }

            }
            for (int i = 0; i < dates.size(); i++) {

                if (NotificationOther != null) {
                    String ds = String.valueOf(events.get(i));
                    if (EventID.equals(String.valueOf(events.get(i)))) {

                        EventModel eventModel = eventModelListFinal.get(i);
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(eventModel.getTimeStamp());

                        Calendar currentCalendarnot = Calendar.getInstance();
                        CalendarDay calendarDaynot = calendarView.getCurrentDate();
                        currentCalendarnot.set(Calendar.DAY_OF_MONTH, calendarDaynot.getDay());
                        currentCalendarnot.set(Calendar.MONTH, calendarDaynot.getMonth());
                        currentCalendarnot.set(Calendar.YEAR, calendarDaynot.getYear());

                        int diff = calendar.get(Calendar.WEEK_OF_YEAR) - currentCalendarnot.get(Calendar.WEEK_OF_YEAR);
                        if (diff > 0) {
                            for (int in = 0; in < diff; in++) {
                                calendarView.goToNext();
                            }
                        } else {
                            for (int in = 0; in < Math.abs(diff); in++) {
                                calendarView.goToPrevious();
                            }
                        }
                        calendarView.setSelectedDate(calendar);
                        Intent in = new Intent(getActivity(), Event_Dialog.class);
                        in.putExtra("SelectedPosition", String.valueOf(i));
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("ArrayList", (ArrayList<? extends Parcelable>) eventModelListFinal);
                        in.putExtra("LIST", bundle);
                        startActivity(in);
                        break;
                    }else{
                        calendarView.setSelectionColor(R.color.lightGray);
                        calendarView.setDateSelected(newCalendar.getTime(), true);
                    }
                } else {
                    calendarView.setSelectionColor(R.color.lightGray);
                    calendarView.setDateSelected(Calendar.getInstance(), true);
                }
            }
            calendarView.addDecorator(new EventDecorator(getResources().getColor(R.color.colorAccent), dates));
            enableMenu();
        } catch (NullPointerException e) {

        } catch (Exception e) {
        }
    }


    public void setNotificationStatus(String  NotificationStatus) {
        this.notification_status = NotificationStatus;
    }

    public void ShowcurrentNotificationDateEvent(){
        try {
            ArrayList<CalendarDay> dates = new ArrayList<>();
            ArrayList<String> events = new ArrayList<>();
            for (EventModel eventModel : eventModelListFinal) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(eventModel.getTimeStamp());
                CalendarDay calendarDay = CalendarDay.from(calendar);
                dates.add(calendarDay);
                String event = String.valueOf(eventModel.getId());
                events.add(event);
            }
            Calendar newCalendar = Calendar.getInstance();
            newCalendar.set(Calendar.HOUR, 0);
            newCalendar.set(Calendar.MINUTE, 0);
            newCalendar.set(Calendar.MILLISECOND, 0);

            Calendar currentCalendar = Calendar.getInstance();
            CalendarDay calendarDay = calendarView.getCurrentDate();
            currentCalendar.set(Calendar.DAY_OF_MONTH, calendarDay.getDay());
            currentCalendar.set(Calendar.MONTH, calendarDay.getMonth());
            currentCalendar.set(Calendar.YEAR, calendarDay.getYear());

            if (NotificationOther == null) {
                int diff = newCalendar.get(Calendar.WEEK_OF_YEAR) - currentCalendar.get(Calendar.WEEK_OF_YEAR);
                if (diff > 0) {
                    for (int ik = 0; ik < diff; ik++) {
                        calendarView.goToNext();
                    }
                } else {
                    for (int ik = 0; ik < Math.abs(diff); ik++) {
                        calendarView.goToPrevious();
                    }
                }
            }
            for (int i = 0; i < dates.size(); i++) {
                if (NotificationOther != null) {
                    if (EventID.equals(String.valueOf(events.get(i)))) {
                        EventModel eventModel = eventModelListFinal.get(i);

                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(eventModel.getTimeStamp());
                        Calendar currentCalendarnot = Calendar.getInstance();
                        CalendarDay calendarDaynot = calendarView.getCurrentDate();
                        currentCalendarnot.set(Calendar.DAY_OF_MONTH, calendarDaynot.getDay());
                        currentCalendarnot.set(Calendar.MONTH, calendarDaynot.getMonth());
                        currentCalendarnot.set(Calendar.YEAR, calendarDaynot.getYear());

                        int diff = calendar.get(Calendar.WEEK_OF_YEAR) - currentCalendarnot.get(Calendar.WEEK_OF_YEAR);
                        if (diff > 0) {
                            for (int in = 0; in < diff; in++) {
                                calendarView.goToNext();
                            }
                        } else {
                            for (int in = 0; in < Math.abs(diff); in++) {
                                calendarView.goToPrevious();
                            }
                        }
                        calendarView.setSelectedDate(calendar);
                        Intent in = new Intent(getActivity(), Event_Dialog.class);
                        in.putExtra("SelectedPosition", String.valueOf(i));
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("ArrayList", (ArrayList<? extends Parcelable>) eventModelListFinal);
                        in.putExtra("LIST", bundle);
                        startActivity(in);
                        break;
                    }else{}
                } else {
                    int Day = newCalendar.get(Calendar.DAY_OF_MONTH);
                    int Month = newCalendar.get(Calendar.MONTH);
                    int FinalMonth;
                    if (Month != 12) {
                        FinalMonth = Month + 1;
                    } else {
                        FinalMonth = Month;
                    }
                    int Year = newCalendar.get(Calendar.YEAR);
                    int dates_array_day = dates.get(i).getDay();
                    int dates_array_Month = dates.get(i).getMonth();
                    int Finaldates_array_Month;
                    if (dates_array_Month != 12) {
                        Finaldates_array_Month = dates_array_Month + 1;
                    } else {
                        Finaldates_array_Month = dates_array_Month;
                    }
                    int dates_array_Year = dates.get(i).getYear();

                   if (Day == dates_array_day && FinalMonth == Finaldates_array_Month && Year == dates_array_Year) {
                        calendarView.setDateSelected(newCalendar.getTime(), true);
                        Intent in = new Intent(getActivity(), Event_Dialog.class);
                        in.putExtra("SelectedPosition", String.valueOf(i));
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("ArrayList", (ArrayList<? extends Parcelable>) eventModelListFinal);
                        in.putExtra("LIST", bundle);
                        startActivity(in);

                        break;
                    } else {
                    calendarView.setSelectionColor(R.color.lightGray);
                    calendarView.setDateSelected(Calendar.getInstance(), true);
                    }
                }
            }
            calendarView.addDecorator(new EventDecorator(getResources().getColor(R.color.colorAccent), dates));
            enableMenu();
        } catch (NullPointerException e) {

        } catch (Exception e) {
        }

    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvRefresh:
                loadData();
                break;
        }
    }
    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        try {
            calendarView.setSelectionColor(R.color.colorAccent);
            long timestamp = date.getCalendar().getTimeInMillis();
            EventModel eventModel = EventStorage.getFilteredEvent(Realm.getDefaultInstance(), timestamp);
            if (eventModel != null) {
                if (!NetworkUtils.isNetworkConnected(getActivity())) {
                    showMessage(getActivity(), getString(R.string.no_network), false);
                    return;
                }
                Intent in = new Intent(getActivity(), Event_Dialog.class);
                in.putExtra("SelectedPosition", String.valueOf(getPosition(eventModel)));
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("ArrayList", (ArrayList<? extends Parcelable>) eventModelListFinal);
                in.putExtra("LIST", bundle);
                startActivity(in);
            } else {
                if (imageAdapter != null)
                    imageAdapter.setEventModelList(new ArrayList<EventModel>());
            }
            Log.i(TAG, "onDateSelected: " + eventModelListFinal.size());
        } catch (NullPointerException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public int getPosition(EventModel eventModel) {
        int position = -1;
        for (int i = 0; i < eventModelListFinal.size(); i++) {
            if (eventModel.getId() == eventModelListFinal.get(i).getId()) {
                position = i;
                break;
            }
        }
        return position;
    }
    public static void ViewPagerOnPageSelected_Dialog(int position) {
        EventModel eventModel = eventModelListFinal.get(position);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(eventModel.getTimeStamp());
        Calendar currentCalendar = Calendar.getInstance();
        CalendarDay calendarDay = calendarView.getCurrentDate();
        currentCalendar.set(Calendar.DAY_OF_MONTH, calendarDay.getDay());
        currentCalendar.set(Calendar.MONTH, calendarDay.getMonth());
        currentCalendar.set(Calendar.YEAR, calendarDay.getYear());

        int diff = calendar.get(Calendar.WEEK_OF_YEAR) - currentCalendar.get(Calendar.WEEK_OF_YEAR);
        if (diff > 0) {
            for (int i = 0; i < diff; i++) {
                calendarView.goToNext();
            }
        } else {
            for (int i = 0; i < Math.abs(diff); i++) {
                calendarView.goToPrevious();
            }
        }
        calendarView.setSelectionColor(R.color.colorAccent);
        calendarView.setSelectedDate(calendar);
    }

    @Override
    public void onResume() {
        devingtabwebview.onResume();
        super.onResume();
    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        devingtabwebview.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        devingtabwebview.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
    }

    @Override
    public void onPageFinished(String url) {
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }
    public static class ImageAdapter extends FragmentPagerAdapter {
        List<EventModel> eventModelListFinal;
        public ImageAdapter(FragmentManager fragmentManager, List<EventModel> eventModelListFinal) {
            super(fragmentManager);
            this.eventModelListFinal = eventModelListFinal;
        }
        public List<EventModel> getEventModelList() {
            return eventModelListFinal;
        }

        public void setEventModelList(List<EventModel> eventModelListFinal) {
            this.eventModelListFinal = eventModelListFinal;
            notifyDataSetChanged();
        }
        @Override
        public int getCount() {
            return eventModelListFinal.size();
        }
        @Override
        public Fragment getItem(int position) {
            return ImageFragment.newInstance(eventModelListFinal.get(position));
        }
    }
}



