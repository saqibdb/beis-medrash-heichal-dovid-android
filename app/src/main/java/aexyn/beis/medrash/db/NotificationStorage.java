package aexyn.beis.medrash.db;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

import aexyn.beis.medrash.model.NotificationModel;
import io.realm.Realm;
import io.realm.Sort;

public class NotificationStorage {

    public static void removeAll() {
        executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(NotificationModel.class).findAll().deleteAllFromRealm();
            }
        });
    }

    public static void save(@NonNull final NotificationModel data) {
        executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(data);
            }
        });
    }

    public static void save(@NonNull final List<NotificationModel> dataList) {
        executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(dataList);
            }
        });
    }

    @NonNull
    public static List<NotificationModel> getAll(@NonNull Realm realm) {
        return realm.where(NotificationModel.class).findAllSorted("id", Sort.DESCENDING);
        //return realm.where(NotificationModel.class).findAll();///AllSorted("id", Sort.DESCENDING);
    }

    private static void executeTransaction(@NonNull Realm.Transaction transaction) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(transaction);
        } catch (Throwable e) {
            Log.e("executeTransaction", e.getMessage());
        } finally {
            close(realm);
        }
    }

    private static void close(@Nullable Realm realm) {
        if (realm != null) {
            realm.close();
        }
    }
}
