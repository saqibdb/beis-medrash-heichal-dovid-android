package aexyn.beis.medrash.db;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import java.util.List;
import aexyn.beis.medrash.model.EventModel;
import io.realm.Realm;

public class EventStorage {

    public static void removeAll() {
        executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(EventModel.class).findAll().deleteAllFromRealm();
            }
        });
    }

    public static void save(@NonNull final EventModel data) {
        executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(data);
            }
        });
    }

    public static void save(@NonNull final List<EventModel> dataList) {
        executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(dataList);
            }
        });
    }

    @NonNull
    public static List<EventModel> getAll(@NonNull Realm realm) {
        return realm.where(EventModel.class).findAllSorted("timeStamp");
        //return realm.where(EventModel.class).findAll();//findAllSorted("timeStamp");
    }

    @NonNull
    public static List<EventModel> getFilteredEvents(@NonNull Realm realm, long timeStamp) {
        return realm.where(EventModel.class).
                equalTo("timeStamp", timeStamp).
                findAll();
    }

    @NonNull
    public static EventModel getFilteredEvent(@NonNull Realm realm, long timeStamp) {
        return realm.where(EventModel.class).
                equalTo("timeStamp", timeStamp).
                findFirst();
    }

    private static void executeTransaction(@NonNull Realm.Transaction transaction) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(transaction);
        } catch (Throwable e) {
            Log.e("executeTransaction", e.getMessage());
        } finally {
            close(realm);
        }
    }

    private static void close(@Nullable Realm realm) {
        if (realm != null) {
            realm.close();
        }
    }
}
