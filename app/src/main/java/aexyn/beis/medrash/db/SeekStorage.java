package aexyn.beis.medrash.db;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

import aexyn.beis.medrash.model.SeekModel;
import io.realm.Realm;

public class SeekStorage {

    public static void removeAll() {
        executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(SeekModel.class).findAll().deleteAllFromRealm();
            }
        });
    }

    public static void save(@NonNull final SeekModel data) {
        executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(data);
            }
        });
    }

    public static void save(@NonNull final List<SeekModel> dataList) {
        executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(dataList);
            }
        });
    }

    @NonNull
    public static List<SeekModel> getAll(@NonNull Realm realm) {
        return realm.where(SeekModel.class).findAll();
    }

    @NonNull
    public static SeekModel getSeek(@NonNull Realm realm, int id) {
        return realm.where(SeekModel.class).
                equalTo("id", id).
                findFirst();
    }

    private static void executeTransaction(@NonNull Realm.Transaction transaction) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(transaction);
        } catch (Throwable e) {
            Log.e("executeTransaction", e.getMessage());
        } finally {
            close(realm);
        }
    }

    private static void close(@Nullable Realm realm) {
        if (realm != null) {
            realm.close();
        }
    }
}
