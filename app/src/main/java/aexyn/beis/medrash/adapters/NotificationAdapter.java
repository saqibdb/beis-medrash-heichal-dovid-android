package aexyn.beis.medrash.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;
import java.util.List;
import aexyn.beis.medrash.R;
import aexyn.beis.medrash.model.NotificationModel;
import static aexyn.beis.medrash.utils.Utils.getDateTime;
import static aexyn.beis.medrash.utils.Utils.getTimeStamp;


public class NotificationAdapter extends ArrayBaseAdapter<NotificationModel> {
    private final Context context;
    List<NotificationModel> listNotificationModel;

    public NotificationAdapter(Context context, List<NotificationModel> listNotificationModel) {
        super(context, listNotificationModel);
        this.context = context;
        this.listNotificationModel = listNotificationModel;
    }

    @Override
    public View getView(LayoutInflater inflater, int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        View v = convertView;
        final NotificationModel object = getItem(position);
        if (v == null) {

            convertView = inflater.inflate(R.layout.list_item_notification, parent, false);

            holder = new ViewHolder();
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            holder.tvMessage = (TextView) convertView.findViewById(R.id.tvMessage);
            holder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvTitle.setText(object.getTitle());
        holder.tvMessage.setText(object.getDescription());

        try {
            holder.tvDate.setText(getDateTime(getTimeStamp(object.getDate())));
        }catch (Exception e){
            holder.tvDate.setText("");
        }

        return convertView;
    }

    @Override
    protected boolean isFilteredBy(NotificationModel object, CharSequence constraint) {
        return false;
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return 0;
            default:
                return 1;
        }
    }
    private class ViewHolder {
        TextView tvTitle, tvMessage, tvDate,mp3id;
    }
}
