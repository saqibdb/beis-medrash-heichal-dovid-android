package aexyn.beis.medrash.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import aexyn.beis.medrash.R;
import aexyn.beis.medrash.model.Child;

public class Sherium_SearchAdapter extends ArrayAdapter {

    private Context context;
    private List<Child> allitemsdata;
    private HashMap<Integer, List<Child>> _listDataChild;
    String _mp3_ID;
    private ArrayList<Child> arraylist;



    public Sherium_SearchAdapter(Context context, List<Child> resource,HashMap<Integer, List<Child>> listChildData,String mp3_ID) {
        super(context, R.layout.sherium_search,resource);
        this.context=context;
        this.allitemsdata=resource;
        this._listDataChild = listChildData;
        this._mp3_ID=mp3_ID;
        this.arraylist = new ArrayList<Child>();
        this.arraylist.addAll(allitemsdata);
    }
    @Override
    public int getCount(){
        return allitemsdata.size();
    }

    @Override
    public Child getItem(int position){
        return allitemsdata.get(position);
    }

    @Override
    public long getItemId(int position){ return position;}

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        View view=convertView;
        if(view==null){
            holder=new ViewHolder();

            LayoutInflater inflater=(LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(R.layout.sherium_search, null);

            holder._Search_Category_Name=(TextView)view.findViewById(R.id.Search_Category_Name);
            holder.Search_Category_id=(TextView)view.findViewById(R.id.id);

            view.setTag(holder);
        } else {
            holder=(ViewHolder)view.getTag();
        }
        holder._Search_Category_Name.setText(allitemsdata.get(position).getName());
        holder.Search_Category_id.setText(String.valueOf(allitemsdata.get(position).getId()));

        return view;
    }

    static class ViewHolder{
        TextView _Search_Category_Name,Search_Category_id;
    }

    public void filter(String charText) {
        try {
            charText = charText.toLowerCase(Locale.getDefault());
            allitemsdata.clear();
            if (charText.length() == 0) {
                allitemsdata.addAll(arraylist);
            }
            else
            {
                for (Child wp : arraylist)
                {
                    if (wp.getName().toLowerCase().contains(charText))
                    {
                        allitemsdata.add(wp);
                    }
                }
            }
            notifyDataSetChanged();
        }catch (Exception ex){}
    }
}