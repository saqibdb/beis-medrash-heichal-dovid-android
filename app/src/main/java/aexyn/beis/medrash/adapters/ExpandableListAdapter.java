package aexyn.beis.medrash.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.HashMap;
import java.util.List;
import aexyn.beis.medrash.R;
import aexyn.beis.medrash.model.Child;
import static aexyn.beis.medrash.utils.Constants.selectedCategory;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

	private Context _context;
	private List<Child> _listDataHeader;
	private HashMap<Integer, List<Child>> _listDataChild;

	public ExpandableListAdapter(Context context, List<Child> listDataHeader,
                                 HashMap<Integer, List<Child>> listChildData) {
		this._context = context;
		this._listDataHeader = listDataHeader;
		this._listDataChild = listChildData;
	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition).getId())
				.get(childPosititon);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

		final Child childText = (Child) getChild(groupPosition, childPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_item, null);
		}

		TextView txtListChild = (TextView) convertView
				.findViewById(R.id.lblListItem);

		txtListChild.setText(childText.getName());
        View layoutParent = convertView.findViewById(R.id.layoutParent);

        View viewLeft = convertView.findViewById(R.id.viewLeft);

        if(selectedCategory == childText.getId()){
            txtListChild.setTextColor(_context.getResources().getColor(R.color.colorPrimaryDark));
            layoutParent.setBackgroundResource(R.color.categories_selected_background);
            viewLeft.setVisibility(View.VISIBLE);
        }else{
            txtListChild.setTextColor(_context.getResources().getColor(R.color.category_item_color));
            layoutParent.setBackgroundResource(R.color.categories_background);
            viewLeft.setVisibility(View.GONE);
        }
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
        try {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition).getId())
                    .size();
        } catch (Exception e) {
            return 0;
        }
    }

	@Override
	public Object getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
		Child headerTitle = (Child) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_group, null);
		}

		View layoutParent = convertView.findViewById(R.id.layoutParent);

		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.lblListHeader);

		ImageView imageView = (ImageView) convertView.findViewById(R.id.imgArrow);

		View viewLeft = convertView.findViewById(R.id.viewLeft);

		if(isExpanded){
			imageView.setImageResource(R.drawable.ic_keyboard_arrow_down);
		}else{
			imageView.setImageResource(R.drawable.ic_keyboard_arrow_right);
		}

        if(selectedCategory == ((Child) getGroup(groupPosition)).getId()){
            lblListHeader.setTextColor(_context.getResources().getColor(R.color.colorPrimaryDark));
            layoutParent.setBackgroundResource(R.color.categories_selected_background);
            viewLeft.setVisibility(View.VISIBLE);
        }else{
            lblListHeader.setTextColor(_context.getResources().getColor(R.color.category_item_color));
            layoutParent.setBackgroundResource(R.color.categories_background);
            viewLeft.setVisibility(View.GONE);
        }

		lblListHeader.setText(headerTitle.getName());

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition)
	{
		return true;
	}

}
