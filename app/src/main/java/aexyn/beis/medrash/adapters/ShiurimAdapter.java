package aexyn.beis.medrash.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.TextView;
import java.util.ArrayList;
import aexyn.beis.medrash.R;
import aexyn.beis.medrash.activities.ShiurimDetailActivity;
import aexyn.beis.medrash.fragments.ListFragment;
import aexyn.beis.medrash.model.ShiurimModel;
import static aexyn.beis.medrash.utils.Utils.getFile;


public class ShiurimAdapter extends ArrayBaseAdapter<ShiurimModel> {

    private final Context context;
    ArrayList<ShiurimModel> listShiurimModel;
    ListFragment listFragment;
    String Nottiposition;

    public ShiurimAdapter(Context context, ArrayList<ShiurimModel> listShiurimModel, ListFragment listFragment) {
        super(context, listShiurimModel);
        this.context = context;
        this.listShiurimModel = listShiurimModel;
        this.listFragment = listFragment;

    }

    int fileDownloading = -1;
    public int getFileDownloading() {
        return fileDownloading;
    }
    public void setFileDownloading(int fileDownloading) {
        this.fileDownloading = fileDownloading;
    }

    @Override
    public View getView(LayoutInflater inflater, final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        View v = convertView;
        final ShiurimModel object = getItem(position);
        SharedPreferences prefs = context.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE);
        Nottiposition = prefs.getString("position", null);

        if (v == null) {
            convertView = inflater.inflate(R.layout.list_item_shiurim, parent, false);
            holder = new ViewHolder();
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            holder.tvMessage = (TextView) convertView.findViewById(R.id.tvDescription);
            holder.tvDuration = (TextView) convertView.findViewById(R.id.tvDDuration);
            holder.imgBtnDownload = (ImageButton) convertView.findViewById(R.id.imgBtnDownload);
            holder.mp3id= (TextView) convertView.findViewById(R.id.mp3id);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvTitle.setText(object.getTitle());
        holder.tvMessage.setText(object.getTopic());
        holder.tvDuration.setText("");
        holder.imgBtnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!getFile(object, context).exists()) {
                    fileDownloading = object.getId();
                    holder.imgBtnDownload.setVisibility(View.GONE);
                    listFragment.downloadFile(object);
                }
            }
        });

        if(object.getId() == fileDownloading) {
            holder.imgBtnDownload.setVisibility(View.INVISIBLE);
            holder.imgBtnDownload.setEnabled(false);
        }else {
            holder.imgBtnDownload.setVisibility(View.VISIBLE);
            holder.imgBtnDownload.setEnabled(true);
        }
        if (getFile(object, context).exists()) {
            holder.imgBtnDownload.setImageResource(R.drawable.ic_download_blue);
        }else{
            holder.imgBtnDownload.setImageResource(R.drawable.ic_download_gray);
        }

        if(Nottiposition!=null) {
            notificationIntent();
            Nottiposition=null;
            SharedPreferences preferences = context.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.clear();
            editor.commit();
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, ShiurimDetailActivity.class)
                        .putParcelableArrayListExtra("shiurimModelList", getList())
                        .putExtra("position",position));
            }
        });

        return convertView;
    }

    public  void notificationIntent(){
        context.startActivity(new Intent(context, ShiurimDetailActivity.class)
                .putParcelableArrayListExtra("shiurimModelList", getList())
                .putExtra("position",Integer.parseInt(Nottiposition)));

    }

    @Override
    protected boolean isFilteredBy(ShiurimModel object, CharSequence constraint) {
        return false;
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return 0;
            default:
                return 1;
        }
    }
    private class ViewHolder {
        TextView tvTitle, tvMessage, tvDuration,mp3id;
        ImageButton imgBtnDownload;
    }

}
