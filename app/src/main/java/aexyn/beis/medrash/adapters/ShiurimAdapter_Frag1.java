package aexyn.beis.medrash.adapters;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import aexyn.beis.medrash.R;
import aexyn.beis.medrash.activities.ShiurimDetailActivity;
import aexyn.beis.medrash.fragments.shiurimFragment_1;
import aexyn.beis.medrash.model.ShiurimModel;
import aexyn.beis.medrash.utils.NetworkUtils;
import aexyn.beis.medrash.utils.PermissionUtility;

import static aexyn.beis.medrash.utils.Utils.getFile;
import static android.content.Context.DOWNLOAD_SERVICE;

public class ShiurimAdapter_Frag1 extends ArrayBaseAdapter<ShiurimModel> {

    private final Context context;
    private List<ShiurimModel> listShiurimModel;
    shiurimFragment_1 listFragment;
    String Nottiposition, Title;
    File mypath;

    public ShiurimAdapter_Frag1(Context context, ArrayList<ShiurimModel> listShiurimModel, List<ShiurimModel> resource, shiurimFragment_1 listFragment) {
        super(context, resource);
        this.context = context;
        this.listShiurimModel = resource;
        this.listFragment = listFragment;
    }

    int fileDownloading = -1;

    public int getFileDownloading() {
        return fileDownloading;
    }

    public void setFileDownloading(int fileDownloading) {
        this.fileDownloading = fileDownloading;
    }

    @Override
    public View getView(LayoutInflater inflater, final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        View v = convertView;
        final ShiurimModel object = getItem(position);
        SharedPreferences prefs = context.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE);
        Nottiposition = prefs.getString("position", null);
        if (v == null) {
            convertView = inflater.inflate(R.layout.list_item_shiurim, parent, false);
            holder = new ViewHolder();
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            holder.tvMessage = (TextView) convertView.findViewById(R.id.tvDescription);
            holder.tvDuration = (TextView) convertView.findViewById(R.id.tvDDuration);
            holder.imgBtnDownload = (ImageButton) convertView.findViewById(R.id.imgBtnDownload);
            holder.mp3id = (TextView) convertView.findViewById(R.id.mp3id);
            holder.download_progbar = (ProgressBar) convertView.findViewById(R.id.download_progbar);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();

        }
        holder.tvTitle.setText(object.getTitle());
        holder.tvMessage.setText(object.getspeaker());
        holder.tvDuration.setText("");

        holder.imgBtnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!getFile(object, context).exists()) {
                    fileDownloading = object.getId();
                    /*holder.imgBtnDownload.setVisibility(View.INVISIBLE);*/
                    holder.download_progbar.setVisibility(View.VISIBLE);
                    downloadFile(object,holder);

                }
            }
        });

        if (object.getId() == fileDownloading) {
            holder.imgBtnDownload.setVisibility(View.INVISIBLE);
            holder.imgBtnDownload.setEnabled(false);
        } else {
            holder.imgBtnDownload.setVisibility(View.VISIBLE);
            holder.imgBtnDownload.setEnabled(true);
        }
        if (getFile(object, context).exists()) {

            holder.imgBtnDownload.setImageResource(R.drawable.ic_download_blue);
            holder.download_progbar.setVisibility(View.GONE);
        } else {
            holder.imgBtnDownload.setImageResource(R.drawable.ic_download_gray);
        }

        if (Nottiposition != null) {

            Title = object.getTitle();
            notificationIntent();
            Nottiposition = null;

            SharedPreferences preferences = context.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.clear();
            editor.commit();
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<ShiurimModel> list = new ArrayList<>();
                if (object.getMp3().contains(".pdf")) {
                    File directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                    mypath = new File(directory, object.getTitle().replace("?", "") + ".pdf");
                    if (mypath.exists()) {
                        view(mypath);
                    } else {
                        downloadFile(object,holder);

                    }
                } else {
                    context.startActivity(new Intent(context, ShiurimDetailActivity.class)
                            .putParcelableArrayListExtra("shiurimModelList", getList())
                            .putExtra("wholeObj", object)
                            .putExtra("position", position));
                }
            }
        });
        return convertView;
    }

    public void view(File PDFpath) {
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent = Intent.createChooser(pdfIntent, "Open File");
        try {
            Uri apkURI = FileProvider.getUriForFile(context, context.getApplicationContext()
                            .getPackageName() + ".app-provider", PDFpath);
            pdfIntent.setDataAndType(apkURI, "application/pdf");
            pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public void downloadFile(ShiurimModel shiurimModel,ViewHolder holder) {
        String type = String.valueOf(shiurimModel.getMp3().substring(shiurimModel.getMp3().length() - 3));
        if (!NetworkUtils.isNetworkConnected(context)) {
            return;
        }
        if (PermissionUtility.checkPermission(context))
            new DownloadFileFromURL(shiurimModel).execute();
            /*if (type.contains("mp3")) {
                new DownloadFileFromURL(shiurimModel).execute();
            } else {
                holder.download_progbar.setVisibility(View.VISIBLE);
                DownloadPDFFromURL(shiurimModel,holder);
            }*/
    }

    public void DownloadPDFFromURL(ShiurimModel shiurimModel,ViewHolder holder) {
        boolean downloading = true;
        DownloadManager downloadManager = (DownloadManager) context.getSystemService(DOWNLOAD_SERVICE);
        Uri Download_Uri = Uri.parse(shiurimModel.getMp3());
        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        String type = String.valueOf(shiurimModel.getMp3().substring(shiurimModel.getMp3().length() - 3));

        if (type.contains("mp3")) {
            request.setTitle(shiurimModel.getTitle() + ".mp3");
        } else {
            request.setTitle(shiurimModel.getTitle() + ".pdf");
        }
        request.setDescription(shiurimModel.getTopic());
        request.setVisibleInDownloadsUi(true);
        if (type.contains("mp3")) {
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, shiurimModel.getTitle().replace("?", "") + ".mp3");
        } else {
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, shiurimModel.getTitle().replace("?", "") + ".pdf");
        }

        long refid = downloadManager.enqueue(request);

        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(refid);
        Cursor cur = null;
        while (downloading) {
            cur = downloadManager.query(query);
            int index = cur.getColumnIndex(DownloadManager.COLUMN_STATUS);
            if (cur.moveToFirst()) {
                if (cur.getInt(index) == DownloadManager.STATUS_SUCCESSFUL) {
                    String downloadLocalUri = cur.getString(cur.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                    String downloadedPackageUriString = cur.getString(index);
                    downloading = false;
                    File directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

                    if (type.contains("mp3")) {
                        mypath = new File(directory, shiurimModel.getTitle().replace("?", "") + ".mp3");
                    } else {
                        mypath = new File(directory, shiurimModel.getTitle().replace("?", "") + ".pdf");
                    }
                    if (mypath.toString().contains(".pdf") && mypath.exists()) {
                        view(mypath);
                        setFileDownloading(-1);
                        notifyDataSetChanged();
                    }else{
                        setFileDownloading(-1);
                        notifyDataSetChanged();
                    }
                   holder.download_progbar.setVisibility(View.GONE);
                }
            }
        }
        cur.close();
        Log.e("sdsd", String.valueOf(refid));

    }

    private class DownloadFileFromURL extends AsyncTask<String, Integer, Boolean> {
        private ShiurimModel shiurimModel;
        private ProgressDialog pDialog;

        DownloadFileFromURL(ShiurimModel shiurimModel) {
            this.shiurimModel = shiurimModel;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... furl) {
            boolean flag = true;
            boolean downloading = true;
            try {
                DownloadManager mManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                Uri Download_Uri = Uri.parse(shiurimModel.getMp3());
                Log.e("adaddsa_Url", Download_Uri.toString());
                DownloadManager.Request mRqRequest = new DownloadManager.Request(Download_Uri);
                mRqRequest.setAllowedOverRoaming(false);
                String type = String.valueOf(shiurimModel.getMp3().substring(shiurimModel.getMp3().length() - 3));

                if (type.contains("mp3")) {
                    mRqRequest.setTitle(shiurimModel.getTitle() + ".mp3");
                } else {
                    mRqRequest.setTitle(shiurimModel.getTitle() + ".pdf");
                }
                mRqRequest.allowScanningByMediaScanner();
                mRqRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                mRqRequest.setDescription(shiurimModel.getTopic());
                if (type.contains("mp3")) {
                    mRqRequest.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, shiurimModel.getTitle().replace("?", "") + ".mp3");
                } else {
                    mRqRequest.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, shiurimModel.getTitle().replace("?", "") + ".pdf");
                }
                mRqRequest.setVisibleInDownloadsUi(true);
                /*DownloadManager.Query query = null;
                query = new DownloadManager.Query();*/
                long refid = mManager.enqueue(mRqRequest);
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(refid);
                Cursor c = null;
               /* if (query != null) {
                    query.setFilterByStatus(DownloadManager.STATUS_FAILED | DownloadManager.STATUS_PAUSED | DownloadManager.STATUS_SUCCESSFUL | DownloadManager.STATUS_RUNNING | DownloadManager.STATUS_PENDING);
                } else {
                    return flag;
                }*/
                while (downloading) {
                    c = mManager.query(query);
                    if (c.moveToFirst()) {
                        Log.i("FLAG", "Downloading");
                        int status = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));

                        if (status == DownloadManager.STATUS_SUCCESSFUL) {
                            Log.i("FLAG", "done");
                            flag = true;
                            break;
                        }
                        if (status == DownloadManager.STATUS_FAILED) {
                            Log.i("FLAG", "Fail");
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            } catch (Exception e) {
                String sf=e.toString();
                Log.e("sddsad",String.valueOf(e));
                Toast.makeText(context,sf,Toast.LENGTH_LONG).show();
                flag = false;
                return flag;
            }
        }

        protected void onProgressUpdate(Integer... progress) {
            //pDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(Boolean downloaded) {
            try {
                if (downloaded) {
                    File directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                    String type = String.valueOf(shiurimModel.getMp3().substring(shiurimModel.getMp3().length() - 3));
                    if (type.contains("mp3")) {
                        mypath = new File(directory, shiurimModel.getTitle().replace("?", "") + ".mp3");
                    } else {
                        mypath = new File(directory, shiurimModel.getTitle().replace("?", "") + ".pdf");
                    }
                    if (mypath.toString().contains(".pdf") && mypath.exists()) {
                        view(mypath);
                        setFileDownloading(-1);
                        notifyDataSetChanged();
                    }else{
                        setFileDownloading(-1);
                        notifyDataSetChanged();
                    }
                } else {
                    Toast.makeText(context, "There was error downloading file. Please try again", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {

            }
        }
    }

    public void notificationIntent() {
        context.startActivity(new Intent(context, ShiurimDetailActivity.class)
                .putParcelableArrayListExtra("shiurimModelList", getList())
                .putExtra("position", Integer.parseInt(Nottiposition)));
    }

    @Override
    protected boolean isFilteredBy(ShiurimModel object, CharSequence constraint) {
        return false;
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return 0;
            default:
                return 1;
        }
    }

    public class ViewHolder {
        TextView tvTitle, tvMessage, tvDuration, mp3id;
        ImageButton imgBtnDownload;
        ProgressBar download_progbar;
    }
}