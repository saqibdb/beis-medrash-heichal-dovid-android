package aexyn.beis.medrash.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class ArrayBaseAdapter<T> extends BaseAdapter implements Filterable {
    private final Object mLock = new Object();
    private LayoutInflater mInflater;
    private ArrayList<T> mObjects;
    private boolean mNotifyOnChange = true;
    private ArrayList<T> mOriginalValues;
    private ArrayFilter mFilter;
    private CharSequence mLastConstraint;
    public ArrayBaseAdapter(Context activity) {
        init(activity, new ArrayList<T>());
    }

    public ArrayBaseAdapter(Context activity, T[] objects) {
        List<T> list = Arrays.asList(objects);
        if (list instanceof ArrayList) {
            init(activity, (ArrayList<T>) list);
        } else {
            init(activity, new ArrayList<T>(list));
        }
    }
    public ArrayBaseAdapter(Context activity, Collection<T> objects) {
        init(activity, new ArrayList<T>(objects));
    }

    public ArrayBaseAdapter() {

    }
    public void add(T object) {
        synchronized (mLock) {
            if (mOriginalValues != null) {
                mOriginalValues.add(object);
                getFilter().filter(mLastConstraint);
            } else {
                mObjects.add(object);
            }
        }
        if (mNotifyOnChange) notifyDataSetChanged();
    }
    public void addAll(Collection<? extends T> collection) {
        boolean isModified;

        synchronized (mLock) {
            if (mOriginalValues != null) {
                isModified = mOriginalValues.addAll(collection);
                if (isModified) getFilter().filter(mLastConstraint);
            } else {
                isModified = mObjects.addAll(collection);
            }
        }
        if (isModified && mNotifyOnChange) notifyDataSetChanged();
    }
    @SafeVarargs
    public final void addAll(T... items) {
        boolean isModified;

        synchronized (mLock) {
            if (mOriginalValues != null) {
                isModified = Collections.addAll(mOriginalValues, items);
                if (isModified) getFilter().filter(mLastConstraint);
            } else {
                isModified = Collections.addAll(mObjects, items);
            }
        }
        if (isModified && mNotifyOnChange) notifyDataSetChanged();
    }
    public void clear() {
        synchronized (mLock) {
            if (mOriginalValues != null) mOriginalValues.clear();
            mObjects.clear();
        }
        if (mNotifyOnChange) notifyDataSetChanged();
    }
    public boolean contains(T object) {
        return mObjects.contains(object);
    }
    public boolean containsAll(Collection<?> collection) {
        return mObjects.containsAll(collection);
    }

    @Override
    public int getCount() {
        return mObjects.size();
    }

    public View getDropDownView(LayoutInflater inflater, int position, View convertView, ViewGroup parent) {
        return getView(inflater, position, convertView, parent);
    }

    @Override
    public final View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getDropDownView(mInflater, position, convertView, parent);
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ArrayFilter();
        }
        return mFilter;
    }

    public ArrayList<T> getFilteredList() {
        ArrayList<T> objects;
        synchronized (mLock) {
            objects = new ArrayList<T>(mObjects);
        }
        return objects;
    }

    @Override
    public T getItem(int position) {
        return mObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public ArrayList<T> getList() {
        ArrayList<T> objects;
        synchronized (mLock) {
            if (mOriginalValues != null) {
                objects = new ArrayList<T>(mOriginalValues);
            } else {
                objects = new ArrayList<T>(mObjects);
            }
        }
        return objects;
    }

    public void setList(Collection<? extends T> objects) {
        synchronized (mLock) {
            if (mOriginalValues != null) {
                mOriginalValues.clear();
                mOriginalValues.addAll(objects);
                getFilter().filter(mLastConstraint);
            } else {
                mObjects.clear();
                mObjects.addAll(objects);
            }
        }
        if (mNotifyOnChange) notifyDataSetChanged();
    }

    public int getPosition(T item) {
        return mObjects.indexOf(item);
    }

    public abstract View getView(LayoutInflater inflater, int position, View convertView,
                                 ViewGroup parent);
    @Override
    public final View getView(int position, View convertView, ViewGroup parent) {
        return this.getView(mInflater, position, convertView, parent);
    }

    private void init(Context context, ArrayList<T> objects) {
        mInflater = LayoutInflater.from(context);
        mObjects = objects;
    }


    protected abstract boolean isFilteredBy(T object, CharSequence constraint);

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        mNotifyOnChange = true;
    }

    public void remove(T object) {
        boolean isModified;

        synchronized (mLock) {
            if (mOriginalValues != null) {
                isModified = mOriginalValues.remove(object);
                if (isModified) getFilter().filter(mLastConstraint);
            } else {
                isModified = mObjects.remove(object);
            }
        }
        if (isModified && mNotifyOnChange) notifyDataSetChanged();
    }

    public void removeAll(Collection<?> collection) {
        boolean isModified;
        synchronized (mLock) {
            if (mOriginalValues != null) {
                isModified = mOriginalValues.removeAll(collection);
                if (isModified) getFilter().filter(mLastConstraint);
            } else {
                isModified = mObjects.removeAll(collection);
            }
        }
        if (isModified && mNotifyOnChange) notifyDataSetChanged();
    }

    public void retainAll(Collection<?> collection) {
        boolean isModified;

        synchronized (mLock) {
            if (mOriginalValues != null) {
                isModified = mOriginalValues.retainAll(collection);
                if (isModified) getFilter().filter(mLastConstraint);
            } else {
                isModified = mObjects.retainAll(collection);
            }
        }
        if (isModified && mNotifyOnChange) notifyDataSetChanged();
    }

    public void setNotifyOnChange(boolean notifyOnChange) {
        mNotifyOnChange = notifyOnChange;
    }

    public void sort() {
        sort(null);
    }

    public void sort(Comparator<? super T> comparator) {
        synchronized (mLock) {
            if (mOriginalValues != null) {
                Collections.sort(mOriginalValues, comparator);
            }
            Collections.sort(mObjects, comparator);
        }
        if (mNotifyOnChange) notifyDataSetChanged();
    }

    public void update(int position, T object) {
        synchronized (mLock) {
            if (mOriginalValues != null) {
                int newPosition = mOriginalValues.indexOf(mObjects.get(position));
                mOriginalValues.set(newPosition, object);
                getFilter().filter(mLastConstraint);
            } else {
                mObjects.set(position, object);
            }
        }
        if (mNotifyOnChange) notifyDataSetChanged();
    }

    private class ArrayFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            final ArrayList<T> values;

            synchronized (mLock) {
                if (TextUtils.isEmpty(constraint)) {
                    if (mOriginalValues != null) {
                        mObjects = new ArrayList<T>(mOriginalValues);
                        mOriginalValues = null;
                    }
                    results.values = mObjects;
                    results.count = mObjects.size();
                    return results;
                } else {
                    if (mOriginalValues == null) {
                        mOriginalValues = new ArrayList<T>(mObjects);
                    }
                    values = new ArrayList<T>(mOriginalValues);
                }
            }

            final ArrayList<T> newValues = new ArrayList<T>();
            for (T value : values) {
                if (isFilteredBy(value, constraint)) {
                    newValues.add(value);
                }
            }

            results.values = newValues;
            results.count = newValues.size();

            return results;
        }
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mLastConstraint = constraint;
            mObjects = (ArrayList<T>) results.values;
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}