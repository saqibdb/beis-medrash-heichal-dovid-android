package aexyn.beis.medrash.pref;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class ReadPref {
    Context ctx;
    private SharedPreferences prefs;

    public ReadPref(Context ctx) {
        this.ctx = ctx;
        prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public String getStringValue(String key) {
        return prefs.getString(key, "");
    }

    public int getIntValue(String key) {
        return prefs.getInt(key, -1);
    }

    public boolean keyExixst(String key) {
        return prefs.contains(key);
    }

    public String getCategory_ForSherium(String key) {
        return prefs.getString(key, "");
    }
}
