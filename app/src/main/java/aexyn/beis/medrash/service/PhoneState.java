package aexyn.beis.medrash.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

public class PhoneState extends Service {
    private CallStateListener mCallStateListener = new CallStateListener();
    private TelephonyManager mTelephonyManager;
    private int mCallState;

    @Override
    public void onCreate() {
        super.onCreate();
        mTelephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        mCallState = mTelephonyManager.getCallState();
        mTelephonyManager.listen(mCallStateListener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    @Override
    public void onDestroy() {
        Log.d("onDestroy", "onDestroy");
        mTelephonyManager.listen(mCallStateListener, PhoneStateListener.LISTEN_NONE);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private final class CallStateListener extends PhoneStateListener {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            sendBroadcast(new Intent("CALL"));
            switch (mCallState) {
                case TelephonyManager.CALL_STATE_IDLE:
                    if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                        Log.d("state", "idle --> off hook = new outgoing call");
                    } else if (state == TelephonyManager.CALL_STATE_RINGING) {
                        Log.d("state", "idle --> ringing = new incoming call");
                    }
                    break;

                case TelephonyManager.CALL_STATE_OFFHOOK:
                    if (state == TelephonyManager.CALL_STATE_IDLE) {
                        Log.d("state", "off hook --> idle  = disconnected");
                    } else if (state == TelephonyManager.CALL_STATE_RINGING) {
                        Log.d("state", "off hook --> ringing = another call waiting");
                    }
                    Log.d("CALL_STATE_OFFHOOK", String.valueOf(state));
                    break;

                case TelephonyManager.CALL_STATE_RINGING:
                    if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                        Log.d("state", "ringing --> off hook = received");
                    } else if (state == TelephonyManager.CALL_STATE_IDLE) {
                        Log.d("state", "ringing --> idle = missed call");
                    }
                    break;
            }
            mCallState = state;
        }
    }
}