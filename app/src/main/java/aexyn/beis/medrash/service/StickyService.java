package aexyn.beis.medrash.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import aexyn.beis.medrash.utils.NotificationUtil;

public class StickyService extends Service {
    private NotificationUtil mNotify;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        mNotify = new NotificationUtil(StickyService.this);
        mNotify.clearNotifications();
        stopSelf();
    }
}