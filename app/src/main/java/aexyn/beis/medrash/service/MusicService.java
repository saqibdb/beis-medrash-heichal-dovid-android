package aexyn.beis.medrash.service;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import java.io.File;
import java.util.ArrayList;
import aexyn.beis.medrash.activities.ShiurimDetailActivity;
import aexyn.beis.medrash.model.ShiurimModel;
import static aexyn.beis.medrash.activities.ShiurimDetailActivity.ACTION_NEXT;
import static aexyn.beis.medrash.utils.Utils.getFile;

public class MusicService extends Service implements
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener, MediaPlayer.OnBufferingUpdateListener {

    private final IBinder musicBind = new MusicBinder();
    ShiurimDetailActivity shiurimDetailActivity;
    private MediaPlayer player;
    private ArrayList<ShiurimModel> ShiurimModels;
    private int ShiurimModelPosn;

    public void onCreate() {
        super.onCreate();
        ShiurimModelPosn = 0;
        player = new MediaPlayer();
        initMusicPlayer();
    }

    public void initMusicPlayer() {
        player.setWakeMode(getApplicationContext(),
                PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        player.setOnPreparedListener(this);
        player.setOnCompletionListener(this);
        player.setOnErrorListener(this);
        player.setOnBufferingUpdateListener(this);
    }
    public void setList(ArrayList<ShiurimModel> theShiurimModels) {
        ShiurimModels = theShiurimModels;
    }
    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        shiurimDetailActivity.onBufferingUpdate(mp, percent);
    }

    //activity will bind to service
    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        player.stop();
        player.release();
        return false;
    }
    public void playShiurimModel(ShiurimModel model) {
        player.reset();
        try {
            final File file = getFile(model, this);
            if (file.exists()) {
                player.setDataSource(file.getAbsolutePath());
            } else {
                player.setDataSource(model.getMp3());
            }
        } catch (Exception e) {
            Log.e("MUSIC SERVICE", "Error setting data source", e);
        }
        player.prepareAsync();
    }
    @Override
    public void onCompletion(MediaPlayer mp) {
        sendBroadcast(new Intent(ACTION_NEXT));
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.v("MUSIC PLAYER", "Playback Error");
        mp.reset();
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        sendBroadcast(new Intent("PREPARE"));
    }
    public int getPosn() {
        try {
            return player.getCurrentPosition();
        } catch (Exception e) {
            return 0;
        }
    }
    public int getDur() {
        try {
            return player.getDuration();
        } catch (Exception e) {
            return 0;
        }
    }
    public boolean isPng() {
        try {
            return player.isPlaying();
        } catch (Exception e) {
            return false;
        }
    }
    public void pausePlayer() {
        try {
            player.pause();
        } catch (Exception e) {

        }
    }
    public void seek(int posn) {
        try {
            player.seekTo(posn);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void go() {
        try {
            player.start();
        } catch (Exception e) {

        }
    }
    @Override
    public void onDestroy() {
        stopForeground(true);
    }

    public class MusicBinder extends Binder {
        public MusicService getService() {
            return MusicService.this;
        }
    }
}
