package aexyn.beis.medrash.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.RemoteViews;
import aexyn.beis.medrash.R;
import aexyn.beis.medrash.activities.ShiurimDetailActivity;
import aexyn.beis.medrash.application.Mp3Application;

import static aexyn.beis.medrash.activities.ShiurimDetailActivity.ACTION_NEXT;
import static aexyn.beis.medrash.activities.ShiurimDetailActivity.ACTION_PLAY_PAUSE;
import static aexyn.beis.medrash.activities.ShiurimDetailActivity.ACTION_PREV;


public class NotificationUtil {
    private static final int NOTIFICATION_ID = 100;
    private final Intent contentIntent;
    private Context mContext;
    private NotificationManager mNotificationManager;
    private PendingIntent pPlayPauseIntent, pPrevIntent, pNextIntent, pContentIntent;

    public NotificationUtil(Context context) {
        mContext = context;
        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        contentIntent = new Intent(context, ShiurimDetailActivity.class);
        pPlayPauseIntent = PendingIntent.getBroadcast(mContext, 0, new Intent(ACTION_PLAY_PAUSE), PendingIntent.FLAG_CANCEL_CURRENT);
        pPrevIntent = PendingIntent.getBroadcast(mContext, 0, new Intent(ACTION_PREV), PendingIntent.FLAG_CANCEL_CURRENT);
        pNextIntent = PendingIntent.getBroadcast(mContext, 0, new Intent(ACTION_NEXT), PendingIntent.FLAG_CANCEL_CURRENT);
        pContentIntent = PendingIntent.getActivity(mContext, 0, contentIntent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    public void showNotificationMessage(String title, String category, boolean isPrevEnable, boolean isNextEanble, int position, boolean isPlaying, boolean isBuffering) {

        Intent homeIntent = new Intent(mContext, ShiurimDetailActivity.class);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        RemoteViews contentView = new RemoteViews(mContext.getPackageName(), R.layout.custom_notification);
        RemoteViews contentViewSmall = new RemoteViews(mContext.getPackageName(), R.layout.custom_notification_small);
        smallNotificationInit(contentViewSmall,title,category,isPlaying,isPrevEnable,isBuffering,isNextEanble);
        LargeNotificationInit(contentView,title,category,isPlaying,isPrevEnable,isBuffering,isNextEanble);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext,
                Mp3Application.NOTIFICATION_CHANNEL_ID);
        notificationBuilder
                .setSmallIcon(R.drawable.ic_stat_name)
                .setUsesChronometer(true)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setContentIntent(pContentIntent)
                .setContentTitle(title)
                .setContentText(category)
                .setOngoing(true)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setVisibility(Notification.VISIBILITY_PUBLIC);
        }
        if (isPlaying && position >= 0) {
            notificationBuilder.setWhen(System.currentTimeMillis() - position).setShowWhen(true).setUsesChronometer(true);
        } else {
            notificationBuilder.setWhen(0).setShowWhen(false).setUsesChronometer(false);
        }
        notificationBuilder.setContent(contentView);
        notificationBuilder.setCustomBigContentView(contentView);
        mNotificationManager.notify(
                NOTIFICATION_ID,
                notificationBuilder.build());
    }

    public void smallNotificationInit(RemoteViews contentViewSmall,String title, String category, boolean isPlaying, boolean isPrevEnable,boolean isBuffering,boolean isNextEanble){
        contentViewSmall.setTextViewText(R.id.notify_title, title);
        contentViewSmall.setTextViewText(R.id.notify_subtitle, category);
        int iconsmal;

        if (isPlaying) {
            iconsmal = R.drawable.ic_pause;
        } else {
            iconsmal = R.drawable.ic_play;
        }

        if (isPrevEnable) {
            contentViewSmall.setViewVisibility(R.id.notify_prev_Rel, View.VISIBLE);
            contentViewSmall.setOnClickPendingIntent(R.id.notify_prev_Rel, pPrevIntent);
        } else {
            contentViewSmall.setViewVisibility(R.id.notify_prev_Rel, View.INVISIBLE);
        }
        contentViewSmall.setImageViewResource(R.id.notify_play_pause, iconsmal);
        contentViewSmall.setOnClickPendingIntent(R.id.notify_play_pause_Rel, pPlayPauseIntent);
        contentViewSmall.setBoolean(R.id.notify_play_pause_Rel, "setEnabled", !isBuffering);
        if (isNextEanble) {
            contentViewSmall.setViewVisibility(R.id.notify_next_Rel, View.VISIBLE);
            contentViewSmall.setOnClickPendingIntent(R.id.notify_next_Rel, pNextIntent);
        } else {
            contentViewSmall.setViewVisibility(R.id.notify_next_Rel, View.INVISIBLE);
        }
    }

    public void LargeNotificationInit(RemoteViews contentView,String title, String category, boolean isPlaying, boolean isPrevEnable,boolean isBuffering,boolean isNextEanble){
        contentView.setTextViewText(R.id.notify_title, title);
        contentView.setTextViewText(R.id.notify_subtitle, category);
        int iconsmal;

        if (isPlaying) {
            iconsmal = R.drawable.ic_pause;
        } else {
            iconsmal = R.drawable.ic_play;
        }
        if (isPrevEnable) {
            contentView.setViewVisibility(R.id.notify_prev_Rel, View.VISIBLE);
            contentView.setOnClickPendingIntent(R.id.notify_prev_Rel, pPrevIntent);
        } else {
            contentView.setViewVisibility(R.id.notify_prev_Rel, View.INVISIBLE);
        }
        contentView.setImageViewResource(R.id.notify_play_pause, iconsmal);
        contentView.setOnClickPendingIntent(R.id.notify_play_pause_Rel, pPlayPauseIntent);
        if (isNextEanble) {
            contentView.setViewVisibility(R.id.notify_next_Rel, View.VISIBLE);
            contentView.setOnClickPendingIntent(R.id.notify_next_Rel, pNextIntent);
        } else {
            contentView.setViewVisibility(R.id.notify_next_Rel, View.INVISIBLE);

        }
    }
    public void clearNotifications() {
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
    }
}
