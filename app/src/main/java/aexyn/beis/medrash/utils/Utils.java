package aexyn.beis.medrash.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.util.Log;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import aexyn.beis.medrash.model.ShiurimModel;

public class Utils {
    public static String getDateTime(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("MMM dd", cal).toString();
        return date;
    }

    public static long getTimeStamp(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = null;
        long time = 0;
        try {
            d = sdf.parse(date);
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            time = c.getTimeInMillis();

        } catch (ParseException e) {
            Log.e("Utils", "getTimeStamp: " + e.getMessage());
        }
        return time;
    }

    public static File getFile(ShiurimModel model, Context context) {
        File directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        if (!directory.exists()) {
            directory.mkdir();
        }
        File mypath;
        if(model.getMp3().contains(".mp3")) {
            mypath = new File(directory, model.getTitle().replace("?", "") + ".mp3");
        }else{
            mypath = new File(directory, model.getTitle().replace("?", "") + ".pdf");
        }
        return mypath;
    }

    public static String getFilePath(String url, Context context){
        File directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        if (!directory.exists()) {
            directory.mkdir();
        }

        String splitURL[] = url.split("=");

        String fileName = splitURL[splitURL.length-1];

        File mypath = new File(directory, fileName);

        return mypath.getAbsolutePath();

    }
    public static int progressToTimer(int progress, int totalDuration) {
        int currentDuration = 0;
        totalDuration = (int) (totalDuration / 1000);
        currentDuration = (int) ((((double)progress) / 100) * totalDuration);
        return currentDuration * 1000;
    }


    public static void showMessage(final Activity context, String message, final boolean toFinish) {
        try {
            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
            alertDialog.setTitle("");
            alertDialog.setMessage(message);
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            if (toFinish)
                                context.finish();
                        }
                    });
            alertDialog.show();
        } catch (Exception e) {
            Log.e("showToast", "Exception --- > " + e.getMessage());
        }
    }
}
