package aexyn.beis.medrash.utils;

/**
 * Created by ahuja on 24/3/17.
 */

public class Constants {
    public static final String SENT_TOKEN_TO_LOCAL = "sentTokenToLocal";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String REGISTRATION_ID = "registrationID";
    public static final String KEY_NOTIFICATION_ID = "notification_id";
    public static final String TOKEN = "token";
    public static final String ACTION = "action";
    public static final String DEVICE_TOKEN = "devicetoken";
    public static final String TYPE = "type";
    public static final String PAGE = "page";
    public static final String CATEGORY_ID = "catId";
    public static final String TITLE = "title";
    public static final String STATUS = "status";
    public static final String MESSAGE = "message";
    public static final String DATA = "data";
    public static final String HOME = "home";
    public static final String ABOUT = "about";
    public static final String ZEMANIM = "zemanim";
    public static final String NOTIFICATIONS = "notifications";
    public static final String ANNOUNCEMENTS = "announcement";
    public static final String WEEKLY_ANNOUNCEMENT = "weekly";
    public static final String EVENTS = "events";
    public static final String SHIURIM = "shiurim";
    public static final String NOTIFICATION_MESSAGE = "notificationMessage";
    public static final String NOTIFICATION_MESSAGE_ForPop = "notificationMessage_ForPop";

    public static int selectedCategory = -1;

    public static enum ACTIONS{
        appuser,
        coreData,
        getcategory,
        getNotifications,
        getEvents,
        getShiurim
    }

    public static enum STATUSES{
        Success,
        Error
    }

}
