/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package aexyn.beis.medrash.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import aexyn.beis.medrash.R;
import aexyn.beis.medrash.activities.MainActivity;
import aexyn.beis.medrash.application.Mp3Application;
import aexyn.beis.medrash.pref.ReadPref;
import aexyn.beis.medrash.pref.SavePref;

import static aexyn.beis.medrash.utils.Constants.KEY_NOTIFICATION_ID;
import static aexyn.beis.medrash.utils.Constants.NOTIFICATIONS;
import static aexyn.beis.medrash.utils.Constants.NOTIFICATION_MESSAGE;
import static aexyn.beis.medrash.utils.Constants.NOTIFICATION_MESSAGE_ForPop;

public class MyGcmListenerService extends GcmListenerService {
    private static final String TAG = "MyGcmListenerService";
    String mp3Id, message;
    Context appContext;

    @Override
    public void onMessageReceived(String from, Bundle data) {
        message = data.getString("message");
        String other = data.getString("other");
        if (other != null && !other.equals("[]")) {
            String[] Other_1 = other.split(",");
            String other1 = Other_1[0];
            String[] Other_2 = other1.split(":");
            mp3Id = Other_2[1];
        }

        appContext = this.getApplicationContext();
        String title = data.getString("title");
        String notificationType = data.getString("notificationType");

        String notificationOther = data.getString("other");

        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);

        new SavePref(this).saveString(NOTIFICATION_MESSAGE_ForPop, message);
        new SavePref(this).remove(NOTIFICATIONS);
        String newMessage;
        if (new ReadPref(this).keyExixst(NOTIFICATION_MESSAGE)) {
            newMessage = new ReadPref(this).getStringValue(NOTIFICATION_MESSAGE) + "\n" + message;
        } else {
            newMessage = message;
        }
        new SavePref(this).saveString(NOTIFICATION_MESSAGE, newMessage);
        //sendNotification(title, new ReadPref(this).getStringValue(NOTIFICATION_MESSAGE),
        // notificationType, notificationOther);
        sendNotification(title, message, notificationType, notificationOther);
    }


    private void sendNotification(String title, String message, String notificationType,
                                  String notificationOther) {
        Intent intent;
        intent = new Intent(this, MainActivity.class);
        intent.putExtra("notificationType", notificationType);
        intent.putExtra("notificationOther", notificationOther);
        intent.putExtra("mp3Id", mp3Id);
        intent.putExtra("message", message);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Bitmap icon = BitmapFactory.decodeResource(this.getResources(),
                R.mipmap.ic_launcher);

        int notificationId = new ReadPref(this).getIntValue(KEY_NOTIFICATION_ID);
        new SavePref(this).saveInt(KEY_NOTIFICATION_ID, notificationId + 1);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(message);
        bigText.setBigContentTitle(title);
        bigText.setSummaryText("more.....");

        notificationId = new ReadPref(this).getIntValue(KEY_NOTIFICATION_ID);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,
                Mp3Application.NOTIFICATION_CHANNEL_ID+notificationId)
                .setStyle(bigText)
                .setLargeIcon(icon)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle(title)
                .setNumber(notificationId)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId, notificationBuilder.build());
    }
}
