package aexyn.beis.medrash;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NotiFication_Dialog_Activity extends AppCompatActivity {
    String message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noti_fication__dialog_);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Intent i = getIntent();
        message = i.getStringExtra("message");
        TextView txt = (TextView) findViewById(R.id.notification_text);
        txt.setText(message);
        Button dismiss_dialog2 = (Button) findViewById(R.id.Dismiss);
        dismiss_dialog2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
