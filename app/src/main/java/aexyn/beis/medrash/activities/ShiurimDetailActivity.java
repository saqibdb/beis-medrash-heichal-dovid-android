package aexyn.beis.medrash.activities;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import aexyn.beis.medrash.R;
import aexyn.beis.medrash.db.SeekStorage;
import aexyn.beis.medrash.model.SeekModel;
import aexyn.beis.medrash.model.ShiurimModel;
import aexyn.beis.medrash.utils.NetworkUtils;
import aexyn.beis.medrash.utils.NotificationUtil;
import aexyn.beis.medrash.utils.PermissionUtility;
import aexyn.beis.medrash.widget.Progressbar;
import io.realm.Realm;
import static aexyn.beis.medrash.utils.PermissionUtility.MY_PERMISSIONS_REQUEST_STORAGE_PERMISSION;
import static aexyn.beis.medrash.utils.PermissionUtility.PERMISSION_PLAY_REQUEST;
import static aexyn.beis.medrash.utils.Utils.getFile;
import static aexyn.beis.medrash.utils.Utils.progressToTimer;
import static aexyn.beis.medrash.utils.Utils.showMessage;

public class ShiurimDetailActivity extends AppCompatActivity implements View.OnClickListener,
        MediaPlayer.OnBufferingUpdateListener,
        SeekBar.OnSeekBarChangeListener,
        AudioManager.OnAudioFocusChangeListener,
        MediaPlayer.OnInfoListener,
        MediaPlayer.OnCompletionListener,
        View.OnTouchListener {
    public static final String ACTION_PLAY_PAUSE = "ACTION_PLAY_PAUSE";
    public static final String ACTION_PREV = "ACTION_PREV";
    public static final String ACTION_NEXT = "ACTION_NEXT";
    public static String TAG = "ShiurimDetailActivity";
    private final Handler handler = new Handler();
    int shiurimPosition;
    ShiurimModel shiurimModel, shiurimModelTmp;
    TextView tvTitle,tvspeaker, tvCategory, tvParsha, tvTopic, tvDuration;
    ImageButton btnPlay, btnPrev, btnNext;
    MediaPlayer player;
    SeekBar songProgressBar;
    boolean prepared = false;
    SeekModel seekModel;
    TextView tvSongStat;
    NotificationUtil mNotify;

    Runnable notification = new Runnable() {
        public void run() {
            primarySeekBarProgressUpdater();
        }
    };
    BroadcastReceiver notifReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (player == null) {
                return;
            }
            if (intent == null) {
                return;
            }
            switch (intent.getAction()) {
                case ACTION_PLAY_PAUSE:
                    if (btnPlay != null) {
                        btnPlay.performClick();
                    }
                    break;

                case ACTION_PREV:
                    if (btnPrev != null) {
                        btnPrev.callOnClick();
                    }
                    break;

                case ACTION_NEXT:
                    if (btnNext != null) {
                        btnNext.callOnClick();
                    }
                    break;

                default:
                    break;

            }
        }
    };
    BroadcastReceiver callReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (player == null) {
                return;
            }
            if (player.isPlaying()) {
                player.pause();
                btnPlay.setImageResource(R.drawable.ic_play);
            }
        }
    };
    private AudioManager mAudioManager;
    private List<ShiurimModel> shiurimModelList;
    private Progressbar progressbar;
    private WifiManager.WifiLock mWifiLock;
    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            checkIfFileDownloaded();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shiurim_detail);
        shiurimModelList = getIntent().getParcelableArrayListExtra("shiurimModelList");
        shiurimPosition = getIntent().getIntExtra("position", 0);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvspeaker= (TextView) findViewById(R.id.tvspeaker);
        tvCategory = (TextView) findViewById(R.id.tvCategory);
        tvParsha = (TextView) findViewById(R.id.tvParsha);
        tvTopic = (TextView) findViewById(R.id.tvTopic);
        tvDuration = (TextView) findViewById(R.id.tvDuration);

        btnPlay = (ImageButton) findViewById(R.id.btnPlay);
        btnPrev = (ImageButton) findViewById(R.id.btnPrev);
        btnNext = (ImageButton) findViewById(R.id.btnNext);
        songProgressBar = (SeekBar) findViewById(R.id.songProgressBar);
        songProgressBar.setOnSeekBarChangeListener(this);

        mWifiLock = ((WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE))
                .createWifiLock(WifiManager.WIFI_MODE_FULL, "Media Player Wi-Fi Lock");

        tvSongStat = (TextView) findViewById(R.id.tvSongStat);
        player = new MediaPlayer();

        findViewById(R.id.btnBack).setOnClickListener(this);
        findViewById(R.id.btnDownload).setOnClickListener(this);
        btnPlay.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        btnNext.setOnClickListener(this);

        mNotify = new NotificationUtil(ShiurimDetailActivity.this);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

        loadShiurim();


        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(downloadReceiver, filter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_PLAY_PAUSE);
        filter.addAction(ACTION_PREV);
        filter.addAction(ACTION_NEXT);
        registerReceiver(notifReceiver, filter);
        registerReceiver(callReceiver, new IntentFilter("CALL"));
    }

    private void loadShiurim() {
        if (!PermissionUtility.checkPermission(ShiurimDetailActivity.this, PERMISSION_PLAY_REQUEST)) {
            return;
        }
        if (shiurimModelList == null) {
            return;
        }
        try {
            shiurimModel = shiurimModelList.get(shiurimPosition);
            String mp3=shiurimModel.getMp3();
            if(mp3.contains(".mp3")) {
                tvTitle.setText(shiurimModel.getTitle());
                tvspeaker.setText(shiurimModel.getspeaker());
                tvCategory.setText(shiurimModel.getCategory());
                tvParsha.setText(shiurimModel.getParsha());
                tvTopic.setText(shiurimModel.getTopic());

                if (isPrevEnabled()) {
                    btnPrev.setAlpha(1f);
                    btnPrev.setEnabled(true);
                } else {
                    btnPrev.setAlpha(0.5f);
                    btnPrev.setEnabled(false);
                }
                if (isNextEnabled()) {
                    btnNext.setAlpha(1f);
                    btnNext.setEnabled(true);
                } else {
                    btnNext.setAlpha(0.5f);
                    btnNext.setEnabled(false);
                }
                checkIfFileDownloaded();
                prepareMp3(shiurimModel);
            }
        } catch (IndexOutOfBoundsException ex) {

        }
    }

    private boolean isPrevEnabled() {
        int tmpIndex = shiurimPosition-1;

        for(int i=tmpIndex; i>=0; i--){
            shiurimModelTmp = shiurimModelList.get(i);
            String mp3=shiurimModelTmp.getMp3();
            if(mp3.contains(".mp3")) {
                return true;
            }
        }
        return false;
    }

    private boolean isNextEnabled() {
        int tmpIndex = shiurimPosition+1;
        for(int i=tmpIndex; i< shiurimModelList.size(); i++){
            shiurimModelTmp = shiurimModelList.get(i);
            String mp3=shiurimModelTmp.getMp3();
            if(mp3.contains(".mp3")) {
                return true;
            }
        }
        return false;
    }

    public void checkIfFileDownloaded() {
        if (getFile(shiurimModel, this).exists()) {
            findViewById(R.id.btnDownload).setVisibility(View.GONE);
        } else {
            findViewById(R.id.btnDownload).setVisibility(View.VISIBLE);
        }
    }

    private void primarySeekBarProgressUpdater() {
        int position = player.getCurrentPosition();
        songProgressBar.setProgress(position);
        SeekStorage.save(new SeekModel(shiurimModel.getId(), player.getCurrentPosition()));

        if (player.isPlaying()) {
            handler.postDelayed(notification, 500);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (player != null) {
            player.stop();
            player.reset();
            player.release();
            player = null;
        }
        handler.removeCallbacks(notification);
        if (mWifiLock.isHeld()) {
            mWifiLock.release();
        }
        try {
            unregisterReceiver(callReceiver);
            unregisterReceiver(notifReceiver);
            unregisterReceiver(downloadReceiver);

        } catch (Exception e) {
            e.printStackTrace();
        }
        mNotify.clearNotifications();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (player != null) {
            if (player.isPlaying()) {
                player.stop();
            }
            player.reset();
            player.release();
            player = null;
        }
        if (mNotify != null) {
            mNotify.clearNotifications();
        }
        handler.removeCallbacks(notification);
        if (mWifiLock.isHeld()) {
            mWifiLock.release();
        }
        try {
            unregisterReceiver(callReceiver);
            unregisterReceiver(notifReceiver);
            unregisterReceiver(downloadReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mNotify.clearNotifications();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                onBackPressed();
                break;
            case R.id.btnDownload:
                if (!NetworkUtils.isNetworkConnected(ShiurimDetailActivity.this)) {
                    showMessage(ShiurimDetailActivity.this, getString(R.string.no_network), false);
                    return;
                }
                if (PermissionUtility.checkPermission(this))
                    new DownloadFileFromURL(shiurimModel).execute();
                break;
            case R.id.btnPlay:
                if (player == null) {
                    return;
                }
                if (player.isPlaying()) {
                    player.pause();
                    btnPlay.setImageResource(R.drawable.ic_play);

                } else {
                    if (prepared) {
                        playSong();
                    } else {
                        prepareMp3(shiurimModel);
                    }
                    Log.i(TAG, "onClick: " + player.getDuration());
                }
                mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getspeaker(), isPrevEnabled(), isNextEnabled(), player.getCurrentPosition(), player.isPlaying(), false);
                break;
            case R.id.btnPrev:
                if (player == null) {
                    return;
                }
                if (player.isPlaying()) {
                    player.stop();
                    btnPlay.setImageResource(R.drawable.ic_play);
                }
                playPrevious();
                break;
            case R.id.btnNext:
                if (player == null) {
                    return;
                }
                if (player.isPlaying()) {
                    player.stop();
                    btnPlay.setImageResource(R.drawable.ic_play);
                }
                playNext();
                break;
        }
    }

    public void showProgressBar() {
        if (progressbar == null)
            progressbar = Progressbar.show(this);
    }

    public void hideProgressBar() {
        if (progressbar != null) {
            progressbar.dismiss();
            progressbar = null;
        }
    }

    public void prepareMp3(final ShiurimModel model) {
        try {
            final File file = getFile(shiurimModel, this);

            if(String.valueOf(file).contains(".mp3")) {

                seekModel = SeekStorage.getSeek(Realm.getDefaultInstance(), shiurimModel.getId());
                player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                setVolumeControlStream(AudioManager.STREAM_MUSIC);

                player.reset();
                if (file.exists()) {
                    player.setDataSource(file.getAbsolutePath());
                    if (mWifiLock.isHeld()) {
                        mWifiLock.release();
                    }
                } else {
                    if (!NetworkUtils.isNetworkConnected(ShiurimDetailActivity.this)) {
                        showMessage(ShiurimDetailActivity.this, getString(R.string.no_network), false);
                        return;
                    }
                    player.setDataSource(model.getMp3());
                    mWifiLock.acquire();
                }
                player.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
                player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        Log.e("MediaPlayer", "Prepared");
                        prepared = true;
                        hideProgressBar();
                        player.setOnCompletionListener(ShiurimDetailActivity.this);

                        int max;
                        final File file = getFile(shiurimModel, ShiurimDetailActivity.this);
                        if (file.exists()) {
                            max = player.getDuration();
                        } else {
                            max = shiurimModel.getDuration() - 1;
                        }
                        songProgressBar.setMax(max);
                        if (seekModel != null) {
                            player.seekTo(seekModel.getDuration());
                            songProgressBar.setProgress(seekModel.getDuration());
                        }
                        showDuration();
                        playSong();
                        mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getspeaker(), isPrevEnabled(), isNextEnabled(), player.getCurrentPosition(), player.isPlaying(), false);

                    }
                });
                player.prepareAsync();

                showProgressBar();
            }else{
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_STORAGE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    new DownloadFileFromURL(shiurimModel).execute();
                }
                break;

            case PERMISSION_PLAY_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadShiurim();
                } else {
                    finish();
                }
                break;
        }

    }


    public void playSong() {

        final File file = getFile(shiurimModel, this);
        if (!file.exists()) {
            if (!NetworkUtils.isNetworkConnected(ShiurimDetailActivity.this)) {
                showMessage(ShiurimDetailActivity.this, getString(R.string.no_network), false);
                return;
            }
            player.setOnBufferingUpdateListener(this);
            player.setOnInfoListener(this);
        } else {

            if (seekModel != null) {
                player.seekTo(seekModel.getDuration());
                songProgressBar.setProgress(seekModel.getDuration());
            }
        }
        player.start();
        primarySeekBarProgressUpdater();
        btnPlay.setImageResource(R.drawable.ic_pause);
        setTimerTask();
    }

    public void setTimerTask() {
        final Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (player != null && player.isPlaying()) {
                            showDuration();
                        } else {
                            timer.cancel();
                            timer.purge();
                        }
                    }
                });

            }
        }, 0, 1000);
    }

    public void showDuration() {
        try {
            tvSongStat.post(new Runnable() {
                @Override
                public void run() {
                    int mediaPosition = player.getCurrentPosition();
                    int durationMinutes = shiurimModel.getDurationMinutes() / 60;
                    int durationSeconds = shiurimModel.getDurationMinutes() % 60;
                    String strDuration = String.format(Locale.getDefault(), "%d:%02d", durationMinutes, durationSeconds);
                    int currentMinutes = mediaPosition / (60 * 1000);
                    int currentSeconds = (mediaPosition / 1000) % 60;
                    String strCurrentPosition = String.format(Locale.getDefault(), "%d:%02d", currentMinutes, currentSeconds);

                    tvSongStat.setText(getString(R.string.song_stat, strCurrentPosition, strDuration));

                    tvDuration.setText(getString(R.string.duration_string, strDuration));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void playNext() {
        if (isNextEnabled()) {
            for(int i=shiurimPosition; i< (shiurimModelList.size()-1); i++){
                shiurimPosition = shiurimPosition + 1;
                shiurimModelTmp = shiurimModelList.get(shiurimPosition);
                String mp3=shiurimModelTmp.getMp3();
                if(mp3.contains(".mp3")) {
                    break;
                }
            }
            loadShiurim();
        }
        mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getspeaker(), isPrevEnabled(), isNextEnabled(), player.getCurrentPosition(), player.isPlaying(), false);
    }

    private void playPrevious() {
        if (isPrevEnabled()) {
            for (int i = shiurimPosition; i > 0; i--) {
                shiurimPosition = shiurimPosition - 1;
                shiurimModelTmp = shiurimModelList.get(shiurimPosition);
                String mp3 = shiurimModelTmp.getMp3();
                if (mp3.contains(".mp3")) {
                    break;
                }
            }
            loadShiurim();
        }
        mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getspeaker(), isPrevEnabled(), isNextEnabled(), player.getCurrentPosition(), player.isPlaying(), false);
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view.getId() == R.id.songProgressBar) {
            Log.e(TAG, "onTouch: " + player.getDuration());
            if (player.isPlaying()) {
                SeekBar sb = (SeekBar) view;
                player.seekTo(sb.getProgress());
                SeekStorage.save(new SeekModel(shiurimModel.getId(), player.getCurrentPosition()));
            }
        }
        return false;
    }


    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        switch (what) {
            case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                Log.e("Buffer", "Started");
                showProgressBar();
                mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getspeaker(), isPrevEnabled(), isNextEnabled(), player.getCurrentPosition(), player.isPlaying(), false);
                break;
            case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                Log.e("Buffer", "Ended");
                hideProgressBar();
                mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getspeaker(), isPrevEnabled(), isNextEnabled(), player.getCurrentPosition(), player.isPlaying(), true);
                break;
        }
        return false;
    }
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        if (fromUser) {
            Log.e("Seekbar", "onProgressChanged");
            player.seekTo(songProgressBar.getProgress());
            primarySeekBarProgressUpdater();
        }
    }
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        handler.removeCallbacks(notification);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        Log.e("Seekbar", "stopTrackingTouch");
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        Log.e("Buffer", "Updated : " + progressToTimer(percent, shiurimModel.getDuration()) + "/" + shiurimModel.getDuration());
        try {
            songProgressBar.setSecondaryProgress(progressToTimer(percent, shiurimModel.getDuration()));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.e("MediaPlayer", "Completion : " + player.getCurrentPosition() + "/" + songProgressBar.getMax());
        try {
            if (songProgressBar.getMax() == player.getCurrentPosition()) {
                btnPlay.setImageResource(R.drawable.ic_play);
                SeekStorage.save(new SeekModel(shiurimModel.getId(), 0));
                seekModel = SeekStorage.getSeek(Realm.getDefaultInstance(), shiurimModel.getId());
                seekModel.setDuration(0);
                prepared = false;
                playNext();
            } else {
                //playNext();
                prepareMp3(shiurimModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        Log.e("MediaPlayer", "AudioFocusChanged");
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                Log.i(TAG, "AUDIOFOCUS_GAIN");
                if (player != null && !player.isPlaying()) {
                    player.start();
                    mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getspeaker(), isPrevEnabled(), isNextEnabled(), player.getCurrentPosition(), player.isPlaying(), false);
                }
                break;
            case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT:
                Log.i(TAG, "AUDIOFOCUS_GAIN_TRANSIENT");
                if (player != null && !player.isPlaying()) {
                    player.start();
                    mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getspeaker(), isPrevEnabled(), isNextEnabled(), player.getCurrentPosition(), player.isPlaying(), false);
                }
                break;
            case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK:
                Log.i(TAG, "AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK");
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                Log.e(TAG, "AUDIOFOCUS_LOSS");
                if (player != null && player.isPlaying()) {
                    player.pause();
                    mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getspeaker(), isPrevEnabled(), isNextEnabled(), player.getCurrentPosition(), player.isPlaying(), false);
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                Log.e(TAG, "AUDIOFOCUS_LOSS_TRANSIENT");
                if (player != null && player.isPlaying()) {
                    player.pause();
                    mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getspeaker(), isPrevEnabled(), isNextEnabled(), player.getCurrentPosition(), player.isPlaying(), false);
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                Log.e(TAG, "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK");
                if (player != null && player.isPlaying()) {
                    player.pause();
                    mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getspeaker(), isPrevEnabled(), isNextEnabled(), player.getCurrentPosition(), player.isPlaying(), false);
                }
                break;
            case AudioManager.AUDIOFOCUS_REQUEST_FAILED:
                Log.e(TAG, "AUDIOFOCUS_REQUEST_FAILED");
                break;
            default:
                break;

        }

    }

    private class DownloadFileFromURL extends AsyncTask<String, Integer, Boolean> {

        private ShiurimModel shiurimModel;
        private ProgressDialog pDialog;
        DownloadFileFromURL(ShiurimModel shiurimModel) {
            this.shiurimModel = shiurimModel;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try{
                findViewById(R.id.btnDownload).setVisibility(View.GONE);
            }catch (Exception e){
            }
        }
        @Override
        protected Boolean doInBackground(String... furl) {
            boolean flag = true;
            boolean downloading =true;
            try{
                DownloadManager mManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                Uri Download_Uri = Uri.parse(shiurimModel.getMp3());
                Log.e("adaddsa_Url", Download_Uri.toString());
                DownloadManager.Request mRqRequest = new DownloadManager.Request(Download_Uri);
                mRqRequest.setAllowedOverRoaming(false);
                String type = String.valueOf(shiurimModel.getMp3().substring(shiurimModel.getMp3().length() - 3));

                if (type.contains("mp3")) {
                    mRqRequest.setTitle(shiurimModel.getTitle() + ".mp3");
                } else {
                    mRqRequest.setTitle(shiurimModel.getTitle() + ".pdf");
                }
                mRqRequest.allowScanningByMediaScanner();
                mRqRequest.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                mRqRequest.setDescription(shiurimModel.getTopic());
                if (type.contains("mp3")) {
                    mRqRequest.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, shiurimModel.getTitle().replace("?", "") + ".mp3");
                } else {
                    mRqRequest.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, shiurimModel.getTitle().replace("?", "") + ".pdf");
                }
                mRqRequest.setVisibleInDownloadsUi(true);
                /*DownloadManager.Query query = null;
                query = new DownloadManager.Query();*/
                long refid = mManager.enqueue(mRqRequest);
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(refid);
                Cursor c = null;
               /* if (query != null) {
                    query.setFilterByStatus(DownloadManager.STATUS_FAILED | DownloadManager.STATUS_PAUSED | DownloadManager.STATUS_SUCCESSFUL | DownloadManager.STATUS_RUNNING | DownloadManager.STATUS_PENDING);
                } else {
                    return flag;
                }*/
                while (downloading) {
                    c = mManager.query(query);
                    if (c.moveToFirst()) {
                        Log.i("FLAG", "Downloading");
                        int status = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));

                        if (status == DownloadManager.STATUS_SUCCESSFUL) {
                            Log.i("FLAG", "done");
                            flag = true;
                            break;
                        }
                        if (status == DownloadManager.STATUS_FAILED) {
                            Log.i("FLAG", "Fail");
                            flag = false;
                            break;
                        }
                    }
                }

                return flag;
            }catch (Exception e) {
                flag = false;
                return flag;
            }
        }
        protected void onProgressUpdate(Integer... progress) {
            pDialog.setProgress(progress[0]);
        }
        @Override
        protected void onPostExecute(Boolean downloaded) {
            try{
                if(downloaded){
                }else{
                }
                checkIfFileDownloaded();
            }catch (Exception e){
            }
        }
    }
}
