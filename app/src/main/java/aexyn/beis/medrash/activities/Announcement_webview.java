package aexyn.beis.medrash.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.MailTo;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.concurrent.ExecutionException;
import aexyn.beis.medrash.BuildConfig;
import aexyn.beis.medrash.R;
import aexyn.beis.medrash.asynctask.AsyncRequest;
import aexyn.beis.medrash.pref.ReadPref;
import aexyn.beis.medrash.pref.SavePref;
import aexyn.beis.medrash.utils.Constants;
import aexyn.beis.medrash.utils.NetworkUtils;
import im.delight.android.webview.AdvancedWebView;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import aexyn.beis.medrash.widget.Progressbar;
import static aexyn.beis.medrash.utils.Constants.ABOUT;
import static aexyn.beis.medrash.utils.Constants.ACTION;
import static aexyn.beis.medrash.utils.Constants.ANNOUNCEMENTS;
import static aexyn.beis.medrash.utils.Constants.DATA;
import static aexyn.beis.medrash.utils.Constants.HOME;
import static aexyn.beis.medrash.utils.Constants.MESSAGE;
import static aexyn.beis.medrash.utils.Constants.STATUS;
import static aexyn.beis.medrash.utils.Constants.TOKEN;
import static aexyn.beis.medrash.utils.Constants.WEEKLY_ANNOUNCEMENT;
import static aexyn.beis.medrash.utils.Constants.ZEMANIM;
import static aexyn.beis.medrash.utils.Utils.showMessage;

public class Announcement_webview extends AppCompatActivity implements AdvancedWebView.Listener, View.OnClickListener {
    public static String TAG = "Announcement_webview";
    AdvancedWebView mWebView;
    TextView tvRefresh;
    SwipeRefreshLayout swipeToRefresh;
    String type;
    private Progressbar progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcemnt_webview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        mWebView = (AdvancedWebView)findViewById(R.id.webview);
        tvRefresh = (TextView)findViewById(R.id.tvRefresh);
        swipeToRefresh = (SwipeRefreshLayout)findViewById(R.id.swipeToRefresh);

        tvRefresh.setOnClickListener(this);
        Intent i=getIntent();
        type= i.getStringExtra("type");

        mWebView.setListener(Announcement_webview.this, this);
        mWebView.getSettings().setAllowContentAccess(true);
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setAppCacheEnabled(true);

        loadData();

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("mailto:")) {
                    MailTo mt = MailTo.parse(url);
                    Intent i = newEmailIntent(Announcement_webview.this, mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc());
                    if (i != null)
                        startActivity(i);
                } else {
                    view.loadUrl(url);
                }
                return true;
            }
        });


        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getSavedPref().remove(type);
                loadData();
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void loadData(){
        if(NetworkUtils.isNetworkConnected(Announcement_webview.this) && !getreadPref().keyExixst(type)){
            load();
        }else {
            loadDataInWebView();
        }
    }

    public void loadDataInWebView(){
        mWebView.loadData(getreadPref().getStringValue(type), "text/html", "UTF-8");
    }


    public void load(){
        if (!NetworkUtils.isNetworkConnected(Announcement_webview.this)) {
            showMessage(Announcement_webview.this, getString(R.string.no_network), false);
            swipeToRefresh.post(new Runnable() {
                @Override
                public void run() {
                    swipeToRefresh.setRefreshing(false);
                }
            });
            return;
        }
        swipeToRefresh.post(new Runnable() {
            @Override
            public void run() {
                swipeToRefresh.setRefreshing(true);
            }
        });

        RequestBody formBody = new FormBody.Builder()
                .add(TOKEN, BuildConfig.token)
                .add(ACTION, Constants.ACTIONS.coreData.name())
                .build();
        new AsyncRequest(BuildConfig.apiDomain, formBody, false, new AsyncRequest.OnTaskCompleted() {
            @Override
            public void onTaskCompleted(AsyncRequest asyncRequest, String result) {
                if (asyncRequest != null) {
                    try {
                        hideError(tvRefresh);
                        result = asyncRequest.get();
                        Log.e(TAG, "" + result);
                        JSONObject jsonObject1 = new JSONObject(result);
                        if (jsonObject1.getString(STATUS).equals(Constants.STATUSES.Success.name())) {
                            getSavedPref().saveString(HOME, jsonObject1.getJSONObject(DATA).getString(HOME));
                            getSavedPref().saveString(ABOUT, jsonObject1.getJSONObject(DATA).getString(ABOUT));
                            getSavedPref().saveString(ZEMANIM, jsonObject1.getJSONObject(DATA).getString(ZEMANIM));
                            getSavedPref().saveString(ANNOUNCEMENTS, jsonObject1.getJSONObject(DATA).getString(ANNOUNCEMENTS));
                            getSavedPref().saveString(WEEKLY_ANNOUNCEMENT, jsonObject1.getJSONObject(DATA).getString(WEEKLY_ANNOUNCEMENT));
                            loadDataInWebView();
                        } else {
                            showError(tvRefresh, jsonObject1.getString(MESSAGE));
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        showError(tvRefresh, getString(R.string.something_went_wrong));
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                        showError(tvRefresh, getString(R.string.something_went_wrong));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        showError(tvRefresh, getString(R.string.something_went_wrong));
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
                swipeToRefresh.post(new Runnable() {
                    @Override
                    public void run() {
                        swipeToRefresh.setRefreshing(false);
                    }
                });
            }

        }).execute();
    }

    public void showError(TextView view, String message){
        view.setVisibility(View.GONE);
        view.setText(message);
    }

    public void hideError(View view){
        view.setVisibility(View.GONE);
    }

    private Intent newEmailIntent(Context context, String address, String subject, String body, String cc) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{address});
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_CC, cc);
        intent.setType("message/rfc822");
        return intent;
    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        super.onPause();
        mWebView.onResume();
    }

    @Override
    public void onDestroy() {
        mWebView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        showProgressBar();
    }

    @Override
    public void onPageFinished(String url) {
        hideProgressBar();
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }

    public SavePref getSavedPref() {
        return new SavePref(Announcement_webview.this);
    }

    public ReadPref getreadPref() {
        return new ReadPref(Announcement_webview.this);
    }

    public void showProgressBar() {
        if (progressbar == null)
            progressbar = Progressbar.show(Announcement_webview.this);
    }

    public void hideProgressBar() {
        if (progressbar != null) {
            progressbar.dismiss();
            progressbar = null;
        }
    }
}
