package aexyn.beis.medrash.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import aexyn.beis.medrash.R;
import aexyn.beis.medrash.db.SeekStorage;
import aexyn.beis.medrash.model.SeekModel;
import aexyn.beis.medrash.model.ShiurimModel;
import aexyn.beis.medrash.service.MusicService;
import aexyn.beis.medrash.service.MusicService.MusicBinder;
import aexyn.beis.medrash.service.StickyService;
import aexyn.beis.medrash.utils.NetworkUtils;
import aexyn.beis.medrash.utils.NotificationUtil;
import aexyn.beis.medrash.utils.PermissionUtility;
import aexyn.beis.medrash.widget.Progressbar;
import io.realm.Realm;
import static aexyn.beis.medrash.utils.PermissionUtility.MY_PERMISSIONS_REQUEST_STORAGE_PERMISSION;
import static aexyn.beis.medrash.utils.PermissionUtility.PERMISSION_PLAY_REQUEST;
import static aexyn.beis.medrash.utils.Utils.getFile;
import static aexyn.beis.medrash.utils.Utils.getFilePath;
import static aexyn.beis.medrash.utils.Utils.progressToTimer;
import static aexyn.beis.medrash.utils.Utils.showMessage;

public class ShiurimDetailMusicServiceActivity extends AppCompatActivity implements View.OnClickListener,
        MediaPlayer.OnBufferingUpdateListener,
        SeekBar.OnSeekBarChangeListener,
        AudioManager.OnAudioFocusChangeListener,
        MediaPlayer.OnInfoListener,
        MediaPlayer.OnCompletionListener,
        View.OnTouchListener,
        MediaController.MediaPlayerControl {

    public static final String ACTION_PLAY_PAUSE = "ACTION_PLAY_PAUSE";
    public static final String ACTION_PREV = "ACTION_PREV";
    public static final String ACTION_NEXT = "ACTION_NEXT";
    public static String TAG = "ShiurimDetailActivity";
    private final Handler handler = new Handler();
    int shiurimPosition;
    ShiurimModel shiurimModel;
    TextView tvTitle, tvCategory, tvParsha, tvTopic, tvDuration;
    ImageButton btnPlay, btnPrev, btnNext;
    SeekBar songProgressBar;
    boolean prepared = false;
    SeekModel seekModel;
    TextView tvSongStat;
    NotificationUtil mNotify;
    BroadcastReceiver notifReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null) {
                return;
            }

            switch (intent.getAction()) {

                case ACTION_PLAY_PAUSE:
                    if (btnPlay != null) {
                        btnPlay.performClick();
                    }
                    break;

                case ACTION_PREV:
                    if (btnPrev != null) {
                        btnPrev.callOnClick();
                    }
                    break;

                case ACTION_NEXT:
                    if (btnNext != null) {
                        btnNext.callOnClick();
                    }
                    break;

                default:
                    break;

            }
        }
    };
    Timer timer = new Timer();
    private MusicService musicSrv;
    private boolean musicBound = false;
    Runnable notification = new Runnable() {
        public void run() {
            primarySeekBarProgressUpdater();
        }
    };
    private Intent playIntent;
    private AudioManager mAudioManager;
    private ArrayList<ShiurimModel> shiurimModelList;
    private Progressbar progressbar;
    BroadcastReceiver onPreparedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            prepared = true;
            hideProgressBar();

            int max;
            final File file = getFile(shiurimModel, ShiurimDetailMusicServiceActivity.this);
            if (file.exists()) {
                max = getDuration();
            } else {
                max = shiurimModel.getDuration() - 1;

            }
            songProgressBar.setMax(max);
            playSong();
        }
    };
    private WifiManager.WifiLock mWifiLock;
    private ServiceConnection musicConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MusicBinder binder = (MusicBinder) service;
            musicSrv = binder.getService();
            musicSrv.setList(shiurimModelList);
            musicBound = true;
            loadShiurim();
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            musicBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shiurim_detail);
        shiurimModelList = getIntent().getParcelableArrayListExtra("shiurimModelList");
        shiurimPosition = getIntent().getIntExtra("position", 0);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvCategory = (TextView) findViewById(R.id.tvCategory);
        tvParsha = (TextView) findViewById(R.id.tvParsha);
        tvTopic = (TextView) findViewById(R.id.tvTopic);
        tvDuration = (TextView) findViewById(R.id.tvDuration);

        btnPlay = (ImageButton) findViewById(R.id.btnPlay);
        btnPrev = (ImageButton) findViewById(R.id.btnPrev);
        btnNext = (ImageButton) findViewById(R.id.btnNext);
        songProgressBar = (SeekBar) findViewById(R.id.songProgressBar);
        songProgressBar.setOnSeekBarChangeListener(this);

        mWifiLock = ((WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE))
                .createWifiLock(WifiManager.WIFI_MODE_FULL, "Media Player Wi-Fi Lock");

        tvSongStat = (TextView) findViewById(R.id.tvSongStat);

        findViewById(R.id.btnBack).setOnClickListener(this);
        findViewById(R.id.btnDownload).setOnClickListener(this);
        btnPlay.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        btnNext.setOnClickListener(this);

        mNotify = new NotificationUtil(ShiurimDetailMusicServiceActivity.this);
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

        if (playIntent == null) {
            playIntent = new Intent(this, MusicService.class);
            bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
            startService(playIntent);
        }
        Intent stickyService = new Intent(this, StickyService.class);
        startService(stickyService);
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_PLAY_PAUSE);
        filter.addAction(ACTION_PREV);
        filter.addAction(ACTION_NEXT);
        registerReceiver(notifReceiver, filter);
        registerReceiver(onPreparedReceiver, new IntentFilter("PREPARE"));
    }

    private void loadShiurim() {

        if (!PermissionUtility.checkPermission(ShiurimDetailMusicServiceActivity.this, PERMISSION_PLAY_REQUEST)) {
            return;
        }

        if (shiurimModelList == null) {
            return;
        }
        try {
            shiurimModel = shiurimModelList.get(shiurimPosition);

            tvTitle.setText(shiurimModel.getTitle());
            tvCategory.setText(shiurimModel.getCategory());
            tvParsha.setText(shiurimModel.getParsha());
            tvTopic.setText(shiurimModel.getTopic());
            if (isPrevEnabled()) {
                btnPrev.setAlpha(1f);
                btnPrev.setEnabled(true);
            } else {
                btnPrev.setAlpha(0.5f);
                btnPrev.setEnabled(false);
            }
            if (isNextEnabled()) {
                btnNext.setAlpha(1f);
                btnNext.setEnabled(true);
            } else {
                btnNext.setAlpha(0.5f);
                btnNext.setEnabled(false);
            }
            checkIfFileDownloaded();
            prepareMp3(shiurimModel);
        } catch (IndexOutOfBoundsException ex) {

        }

    }

    private boolean isPrevEnabled() {
        return shiurimPosition > 0;
    }

    private boolean isNextEnabled() {
        return shiurimPosition < (shiurimModelList.size() - 1);
    }

    public void checkIfFileDownloaded() {
        if (getFile(shiurimModel, this).exists()) {
            findViewById(R.id.btnDownload).setVisibility(View.GONE);
        } else {
            findViewById(R.id.btnDownload).setVisibility(View.VISIBLE);
        }
    }

    private void primarySeekBarProgressUpdater() {

        int position = getCurrentPosition();
        songProgressBar.setProgress(position);
        SeekStorage.save(new SeekModel(shiurimModel.getId(), getCurrentPosition()));

        if (isPlaying()) {
            handler.postDelayed(notification, 500);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        stopService(playIntent);
        handler.removeCallbacks(notification);
        if (mWifiLock.isHeld()) {
            mWifiLock.release();
        }
        try {
            unregisterReceiver(onPreparedReceiver);
            unregisterReceiver(notifReceiver);
            timer.cancel();
            timer.purge();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mNotify.clearNotifications();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        stopService(playIntent);
        if (mNotify != null) {
            mNotify.clearNotifications();
        }
        handler.removeCallbacks(notification);
        if (mWifiLock.isHeld()) {
            mWifiLock.release();

        }
        try {
            unregisterReceiver(onPreparedReceiver);
            unregisterReceiver(notifReceiver);
            timer.cancel();
            timer.purge();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mNotify.clearNotifications();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                onBackPressed();
                break;
            case R.id.btnDownload:
                if (!NetworkUtils.isNetworkConnected(ShiurimDetailMusicServiceActivity.this)) {
                    showMessage(ShiurimDetailMusicServiceActivity.this, getString(R.string.no_network), false);
                    return;
                }
                if (PermissionUtility.checkPermission(this))
                    downloadFile(shiurimModel);
                break;
            case R.id.btnPlay:
                if (isPlaying()) {
                    pause();
                    btnPlay.setImageResource(R.drawable.ic_play);

                } else {
                    if (prepared) {
                        playSong();
                    } else {
                        prepareMp3(shiurimModel);
                    }
                    Log.i(TAG, "onClick: " + getDuration());
                }
                mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getTopic(), isPrevEnabled(), isNextEnabled(), getCurrentPosition(), isPlaying(), false);
                break;

            case R.id.btnPrev:
                if (isPlaying()) {
                    btnPlay.setImageResource(R.drawable.ic_play);
                }
                playPrevious();
                break;

            case R.id.btnNext:
                if (isPlaying()) {
                    btnPlay.setImageResource(R.drawable.ic_play);
                }
                playNext();
                break;
        }
    }

    public void showProgressBar() {
        if (progressbar == null)
            progressbar = Progressbar.show(this);
    }

    public void hideProgressBar() {
        if (progressbar != null) {
            progressbar.dismiss();
            progressbar = null;
        }
    }
    public void prepareMp3(final ShiurimModel model) {
        try {
            seekModel = SeekStorage.getSeek(Realm.getDefaultInstance(), shiurimModel.getId());
            setVolumeControlStream(AudioManager.STREAM_MUSIC);
            showProgressBar();
            musicSrv.playShiurimModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_STORAGE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    downloadFile(shiurimModel);
                }
                break;

            case PERMISSION_PLAY_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadShiurim();
                } else {
                    finish();
                }
                break;
        }

    }

    public void playSong() {

        final File file = getFile(shiurimModel, this);
        if (!file.exists()) {
            if (!NetworkUtils.isNetworkConnected(ShiurimDetailMusicServiceActivity.this)) {
                showMessage(ShiurimDetailMusicServiceActivity.this, getString(R.string.no_network), false);
                return;
            }
        }
        if (seekModel != null) {
            seekTo(seekModel.getDuration());
            songProgressBar.setProgress(seekModel.getDuration());
        }
        start();
        primarySeekBarProgressUpdater();
        btnPlay.setImageResource(R.drawable.ic_pause);
        setTimerTask();
        mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getTopic(), isPrevEnabled(), isNextEnabled(), getCurrentPosition(), isPlaying(), false);
    }

    public void setTimerTask() {
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isPlaying()) {
                            showDuration();
                        } else {
                            timer.cancel();
                            timer.purge();
                        }
                    }
                });

            }
        }, 0, 1000);
    }

    public void showDuration() {
        try {
            tvSongStat.post(new Runnable() {
                @Override
                public void run() {
                    int mediaPosition = getCurrentPosition();
                    int durationMinutes = shiurimModel.getDurationMinutes() / 60;
                    int durationSeconds = shiurimModel.getDurationMinutes() % 60;
                    String strDuration = String.format(Locale.getDefault(), "%d:%02d", durationMinutes, durationSeconds);
                    int currentMinutes = mediaPosition / (60 * 1000);
                    int currentSeconds = (mediaPosition / 1000) % 60;
                    String strCurrentPosition = String.format(Locale.getDefault(), "%d:%02d", currentMinutes, currentSeconds);

                    tvSongStat.setText(getString(R.string.song_stat, strCurrentPosition, strDuration));
                    tvDuration.setText(getString(R.string.duration_string, strDuration));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void downloadFile(ShiurimModel shiurimModel) {
        new DownloadFileFromURL(shiurimModel.getMp3(), shiurimModel.getFileSize()).execute();
    }


    private void playNext() {
        if (isNextEnabled()) {
            shiurimPosition++;
            loadShiurim();
        }
        mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getTopic(), isPrevEnabled(), isNextEnabled(), getCurrentPosition(), isPlaying(), false);
    }

    private void playPrevious() {
        if (isPrevEnabled()) {
            shiurimPosition--;
            loadShiurim();
        }
        mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getTopic(), isPrevEnabled(), isNextEnabled(), getCurrentPosition(), isPlaying(), false);
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view.getId() == R.id.songProgressBar) {
            Log.e(TAG, "onTouch: " + getDuration());
            if (isPlaying()) {
                SeekBar sb = (SeekBar) view;
                seekTo(sb.getProgress());
                SeekStorage.save(new SeekModel(shiurimModel.getId(), getCurrentPosition()));
            }
        }
        return false;
    }


    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        switch (what) {
            case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                Log.e("Buffer", "Started");
                showProgressBar();
                mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getTopic(), isPrevEnabled(), isNextEnabled(), getCurrentPosition(), isPlaying(), false);
                break;
            case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                Log.e("Buffer", "Ended");
                hideProgressBar();
                mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getTopic(), isPrevEnabled(), isNextEnabled(), getCurrentPosition(), isPlaying(), true);
                break;
        }
        return false;
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            Log.e("Seekbar", "onProgressChanged");
            seekTo(songProgressBar.getProgress());
            primarySeekBarProgressUpdater();
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        handler.removeCallbacks(notification);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        Log.e("Seekbar", "stopTrackingTouch");

    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        Log.e("Buffer", "Updated : " + progressToTimer(percent, shiurimModel.getDuration()) + "/" + shiurimModel.getDuration());
        try {
            songProgressBar.setSecondaryProgress(progressToTimer(percent, shiurimModel.getDuration()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.e("MediaPlayer", "Completion : " + getCurrentPosition() + "/" + songProgressBar.getMax());
        try {
            if (songProgressBar.getMax() == getCurrentPosition()) {
                btnPlay.setImageResource(R.drawable.ic_play);
                SeekStorage.save(new SeekModel(shiurimModel.getId(), 0));
                seekModel = SeekStorage.getSeek(Realm.getDefaultInstance(), shiurimModel.getId());
                seekModel.setDuration(0);
                prepared = false;
                playNext();
            } else {
                prepareMp3(shiurimModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onAudioFocusChange(int focusChange) {
        Log.e("MediaPlayer", "AudioFocusChanged");
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                Log.i(TAG, "AUDIOFOCUS_GAIN");
                start();
                mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getTopic(), isPrevEnabled(), isNextEnabled(), getCurrentPosition(), isPlaying(), false);
                break;
            case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT:
                Log.i(TAG, "AUDIOFOCUS_GAIN_TRANSIENT");
                start();
                mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getTopic(), isPrevEnabled(), isNextEnabled(), getCurrentPosition(), isPlaying(), false);
                break;
            case AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK:
                Log.i(TAG, "AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK");
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                Log.e(TAG, "AUDIOFOCUS_LOSS");
                pause();
                mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getTopic(), isPrevEnabled(), isNextEnabled(), getCurrentPosition(), isPlaying(), false);
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                Log.e(TAG, "AUDIOFOCUS_LOSS_TRANSIENT");
                pause();
                mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getTopic(), isPrevEnabled(), isNextEnabled(), getCurrentPosition(), isPlaying(), false);
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                Log.e(TAG, "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK");
                pause();
                mNotify.showNotificationMessage(shiurimModel.getTitle(), shiurimModel.getTopic(), isPrevEnabled(), isNextEnabled(), getCurrentPosition(), isPlaying(), false);
                break;
            case AudioManager.AUDIOFOCUS_REQUEST_FAILED:
                Log.e(TAG, "AUDIOFOCUS_REQUEST_FAILED");
                break;
            default:
                break;
        }
    }

    @Override
    public void start() {
        musicSrv.go();
    }

    @Override
    public void pause() {
        musicSrv.pausePlayer();
    }

    @Override
    public int getDuration() {
        if (musicSrv != null && musicBound && musicSrv.isPng())
            return musicSrv.getDur();
        else return 0;
    }
    @Override
    public int getCurrentPosition() {
        return musicSrv.getPosn();
    }

    @Override
    public void seekTo(int pos) {
        musicSrv.seek(pos);
    }

    @Override
    public boolean isPlaying() {
        if (musicSrv != null && musicBound)
            return musicSrv.isPng();
        return false;
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        return 0;
    }

    private class DownloadFileFromURL extends AsyncTask<String, Integer, String> {

        private String URL = "";
        private int fileSize = 0;
        private ProgressDialog pDialog;

        DownloadFileFromURL(String URL, int fileSize) {
            this.URL = URL;
            this.fileSize = fileSize;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ShiurimDetailMusicServiceActivity.this);
            pDialog.setMessage(getString(R.string.file_download_string));
            pDialog.setIndeterminate(false);
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... furl) {
            int count;
            try {
                URL url = new URL(URL);
                Log.e("adaddsa_Url",url.toString());
                HttpURLConnection conection = (HttpURLConnection) url.openConnection();
                conection.setRequestMethod("GET");
                conection.setRequestProperty("Accept-Encoding", "identity");
                conection.connect();

                int lenghtOfFile = fileSize;
                pDialog.setMax(100);
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                OutputStream output = new FileOutputStream(getFilePath(URL, ShiurimDetailMusicServiceActivity.this));
                byte data[] = new byte[1024];
                double total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    int percentage = (int) ((total / lenghtOfFile) * 100);
                    publishProgress(percentage);
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
            pDialog.setProgress(progress[0]);
        }
        @Override
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            checkIfFileDownloaded();
        }
    }
}
