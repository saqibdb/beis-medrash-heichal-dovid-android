package aexyn.beis.medrash.activities;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.concurrent.ExecutionException;
import aexyn.beis.medrash.BuildConfig;
import aexyn.beis.medrash.R;
import aexyn.beis.medrash.adapters.ExpandableListAdapter;
import aexyn.beis.medrash.asynctask.AsyncRequest;
import aexyn.beis.medrash.fragments.BaseFragment;
import aexyn.beis.medrash.fragments.Contact_Fragment;
import aexyn.beis.medrash.fragments.DonateFragment;
import aexyn.beis.medrash.fragments.EventsFragment;
import aexyn.beis.medrash.fragments.ListFragment;
import aexyn.beis.medrash.fragments.shiurimFragment_1;
import aexyn.beis.medrash.fragments.WebviewFragment;
import aexyn.beis.medrash.gcm.RegistrationIntentService;
import aexyn.beis.medrash.pref.ReadPref;
import aexyn.beis.medrash.pref.SavePref;
import aexyn.beis.medrash.utils.Constants;
import aexyn.beis.medrash.widget.Progressbar;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import static aexyn.beis.medrash.utils.Constants.ACTION;
import static aexyn.beis.medrash.utils.Constants.DEVICE_TOKEN;
import static aexyn.beis.medrash.utils.Constants.HOME;
import static aexyn.beis.medrash.utils.Constants.KEY_NOTIFICATION_ID;
import static aexyn.beis.medrash.utils.Constants.NOTIFICATION_MESSAGE_ForPop;
import static aexyn.beis.medrash.utils.Constants.REGISTRATION_COMPLETE;
import static aexyn.beis.medrash.utils.Constants.REGISTRATION_ID;
import static aexyn.beis.medrash.utils.Constants.SENT_TOKEN_TO_LOCAL;
import static aexyn.beis.medrash.utils.Constants.SENT_TOKEN_TO_SERVER;
import static aexyn.beis.medrash.utils.Constants.STATUS;
import static aexyn.beis.medrash.utils.Constants.TOKEN;
import static aexyn.beis.medrash.utils.Constants.TYPE;

public class MainActivity extends AppCompatActivity implements BaseFragment.OnFragmentInteractionListener, View.OnClickListener {

    private static final String TAG = "MainActivity";
    static TextView tvTitle;
    RadioGroup radioGroup;
    RadioButton rbHome;
    RadioButton rbEvent;
    RadioButton rbShiurim;
    RadioButton rbDonate;
    RadioButton rbContact;
    RadioButton most_recent_shiurim;
    static ImageButton btnSearch;
    ImageButton backMenu;
    SharedPreferences sharedPreferences;
    String regID = "";
    ExpandableListAdapter listAdapter;
    static View layoutToolbar;
    SearchView searchView;
    static String title = "";
    static ViewPager viewPager;
    static ViewPagerAdapter viewPagerAdapter;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private Progressbar progressbar;
    String notificationOther;
    static String webview_shiurim_check = "";
    static String mp3Id = "";
    static String Selected_title = "";
    static boolean firsttime = true;
    String newNotifyMessage,Notification_message;
    static Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvTitle = (TextView) findViewById(R.id.tvTitle);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        rbHome = (RadioButton) findViewById(R.id.rbHome);
        rbEvent = (RadioButton) findViewById(R.id.rbEvent);
        rbShiurim = (RadioButton) findViewById(R.id.rbShiurim);
        most_recent_shiurim = (RadioButton) findViewById(R.id.most_recent_shiurim);
        rbDonate = (RadioButton) findViewById(R.id.rbDonate);
        rbContact = (RadioButton) findViewById(R.id.rbContact);
        backMenu = (ImageButton) findViewById(R.id.backMenu);
        btnSearch = (ImageButton) findViewById(R.id.btnSearch);
        searchView = (SearchView) findViewById(R.id.searchView);
        layoutToolbar = findViewById(R.id.layoutToolbar);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        ctx = MainActivity.this;

        Selected_title = "Shiurim";

        if (getIntent().hasExtra("notificationOther")) {
            notificationOther = getIntent().getStringExtra("notificationOther");
        }

        if (getIntent().hasExtra("mp3Id")) {
            mp3Id = getIntent().getStringExtra("mp3Id");
        }

        if(getIntent().hasExtra("message")){
            Notification_message = getIntent().getStringExtra("message");
        }


        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), MainActivity.this);
        viewPager.setAdapter(viewPagerAdapter);

        new SavePref(this).saveInt(KEY_NOTIFICATION_ID, 0);

        btnSearch.setOnClickListener(this);

        viewPager.setOffscreenPageLimit(5);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        rbHome.post(new Runnable() {
                            @Override
                            public void run() {
                                rbHome.setChecked(true);
                            }
                        });
                        break;
                    case 1:
                        rbShiurim.post(new Runnable() {
                            @Override
                            public void run() {
                                rbShiurim.setChecked(true);
                            }
                        });
                        break;

                    case 2:
                        rbEvent.post(new Runnable() {
                            @Override
                            public void run() {
                                rbEvent.setChecked(true);
                            }
                        });
                        break;
                    case 3:
                        rbContact.post(new Runnable() {
                            @Override
                            public void run() {
                                rbContact.setChecked(true);
                            }
                        });
                        break;
                    case 4:
                        rbDonate.post(new Runnable() {
                            @Override
                            public void run() {
                                rbDonate.setChecked(true);
                            }
                        });
                        break;
                    case 5:
                        if (webview_shiurim_check.equals("")) {
                            homeClicked();
                        } else {
                            most_recent_shiurim.post(new Runnable() {
                                @Override
                                public void run() {
                                    most_recent_shiurim.setChecked(true);
                                }
                            });

                        }
                        break;
                    default:
                        rbHome.post(new Runnable() {
                            @Override
                            public void run() {
                                rbHome.setChecked(true);
                            }
                        });
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                InputMethodManager in = (InputMethodManager) MainActivity.this.getSystemService(INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(radioGroup.getWindowToken(), 0);

                btnSearch.setVisibility(View.GONE);
                switch (i) {
                    case R.id.rbHome:

                        homeClicked();

                        break;
                    case R.id.rbShiurim:

                        shiurimClicked("");

                        break;

                    case R.id.rbEvent:

                        eventClicked();

                        break;

                    case R.id.rbContact:

                        contactClicked();
                        break;
                    case R.id.rbDonate:

                        donateClicked();
                        break;
                    case R.id.most_recent_shiurim:
                        shiurimClicked_weview("");
                        break;
                }
            }
        });

        if (getIntent().hasExtra("notificationType")) {
            switch (getIntent().getStringExtra("notificationType")) {
                case "normal":

                    normalNotificationType();
                    break;
                case "shiurim-normal":

                    rbShiurim.post(new Runnable() {
                        @Override
                        public void run() {
                            rbShiurim.setChecked(true);
                        }
                    });
                    normalNotificationType();
                    break;
                case "events-normal":
                    rbEvent.post(new Runnable() {
                        @Override
                        public void run() {
                            rbEvent.setChecked(true);
                        }
                    });

                    normalNotificationType();
                    break;
                case "shiurim":
                    Fragment fragment = viewPagerAdapter.getItem(viewPager.getCurrentItem());
                    if (fragment instanceof EventsFragment) {
                        title = "";
                        ((EventsFragment) fragment).setNotificationStatus("Notification_Arrived");

                    }
                    rbShiurim.post(new Runnable() {
                        @Override
                        public void run() {
                            rbShiurim.setChecked(true);
                        }
                    });

                    break;
                case "events":
                    rbEvent.post(new Runnable() {
                        @Override
                        public void run() {
                            rbEvent.setChecked(true);
                        }
                    });
                    break;
                default:
                    rbHome.post(new Runnable() {
                        @Override
                        public void run() {
                            rbHome.setChecked(true);
                        }
                    });
            }
        } else {
            rbHome.post(new Runnable() {
                @Override
                public void run() {
                    rbHome.setChecked(true);
                }
            });
        }

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                boolean sentToken = sharedPreferences
                        .getBoolean(SENT_TOKEN_TO_LOCAL, false);
                if (sentToken) {
                    Log.e(TAG, "Token retrieved and sent to server! You can now use gcmsender to\n" +
                            "        send downstream messages to this app.");
                    regID = sharedPreferences.getString(REGISTRATION_ID, "");
                    sendTokenToServer();
                    Log.i(TAG, "Registration ID From Received Intent------  " + regID);
                } else {
                    Log.e(TAG, "An error occurred while either fetching the InstanceID token,\n" +
                            "        sending the fetched token to the server or subscribing to the PubSub topic. Please try\n" +
                            "        running the sample again.");
                }
            }
        };
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = viewPagerAdapter.getItem(viewPager.getCurrentItem());
        if (fragment instanceof shiurimFragment_1) {
            webview_shiurim_check = "";
            title = "";
            ((shiurimFragment_1) fragment).backpressed();
        } else {
            finish();
        }
    }

    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Fragment fragment = viewPagerAdapter.getItem(viewPager.getCurrentItem());
            if (fragment instanceof ListFragment) {
                title = "";
                ((ListFragment) fragment).notifyDataChange();
            }
        }
    };

    public static void homeClicked() {
        webview_shiurim_check = "";
        viewPager.setCurrentItem(0);
        layoutToolbar.setVisibility(View.VISIBLE);
        tvTitle.setText("Home");
        Selected_title = "Home";
    }

    public static void shiurimClicked(String val) {
        if (webview_shiurim_check.equals("")) {
            if (mp3Id == null || mp3Id.equals("")) {
                layoutToolbar.setVisibility(View.GONE);
                tvTitle.setText("Shiurim");
                Selected_title = "Shiurim";
                btnSearch.setVisibility(View.VISIBLE);
                viewPager.setCurrentItem(1);

                Fragment fragment = viewPagerAdapter.getItem(viewPager.getCurrentItem());
                if (fragment instanceof shiurimFragment_1) {
                    title = "";
                    ((shiurimFragment_1) fragment).setPageNum(1);
                    ((shiurimFragment_1) fragment).setCategoryID("");
                    ((shiurimFragment_1) fragment).most_recent_shiurim_bt_VISIBLE();
                    ((shiurimFragment_1) fragment).getCategories();
                }
            } else {
                layoutToolbar.setVisibility(View.GONE);
                tvTitle.setText("Shiurim");
                Selected_title = "Shiurim";
                btnSearch.setVisibility(View.VISIBLE);
                viewPager.setCurrentItem(1);

                Fragment fragment = viewPagerAdapter.getItem(viewPager.getCurrentItem());
                if (fragment instanceof shiurimFragment_1) {
                    title = "";
                    ((shiurimFragment_1) fragment).setPageNum(1);
                    ((shiurimFragment_1) fragment).setCategoryID("");
                    ((shiurimFragment_1) fragment).most_recent_shiurim_bt_VISIBLE();
                    ((shiurimFragment_1) fragment).loadData("");
                }

            }
        } else {}
    }

    public void eventClicked() {
        webview_shiurim_check = "";
        viewPager.setCurrentItem(2);
        layoutToolbar.setVisibility(View.VISIBLE);
        tvTitle.setText(getString(R.string.title_events));
        Selected_title = getString(R.string.title_events);
    }

    public void contactClicked() {
        webview_shiurim_check = "";
        viewPager.setCurrentItem(3);
        layoutToolbar.setVisibility(View.VISIBLE);
        tvTitle.setText(getString(R.string.title_contact));
        Selected_title = getString(R.string.title_contact);
    }

    public void donateClicked() {
        webview_shiurim_check = "";
        viewPager.setCurrentItem(4);
        layoutToolbar.setVisibility(View.VISIBLE);
        tvTitle.setText(getString(R.string.title_donate));
        Selected_title = getString(R.string.title_donate);
    }

    public static void shiurimClicked_weview(String val1) {
        webview_shiurim_check = val1;
        viewPager.setCurrentItem(5);
        layoutToolbar.setVisibility(View.GONE);
        tvTitle.setText("Shiurim");
        Selected_title = "Shiurim";
        btnSearch.setVisibility(View.VISIBLE);

        Fragment fragment = viewPagerAdapter.getItem(viewPager.getCurrentItem());
        if (fragment instanceof shiurimFragment_1) {
            title = "";
            ((shiurimFragment_1) fragment).setPageNum(1);
            ((shiurimFragment_1) fragment).setCategoryID("");
            ((shiurimFragment_1) fragment).setwebview_shiurim_check("true");
            ((shiurimFragment_1) fragment).most_recent_shiurim_bt_HIDE();
            ((shiurimFragment_1) fragment).loadData("");
            firsttime = true;
        }
    }
    public void sendTokenToServer() {
        if (!sharedPreferences.getBoolean(SENT_TOKEN_TO_SERVER, false)) {
            RequestBody formBody = new FormBody.Builder()
                    .add(TOKEN, BuildConfig.token)
                    .add(ACTION, Constants.ACTIONS.appuser.name())
                    .add(DEVICE_TOKEN, regID)
                    .add(TYPE, "android")
                    .build();
            new AsyncRequest(BuildConfig.apiDomain, formBody, false, new AsyncRequest.OnTaskCompleted() {
                @Override
                public void onTaskCompleted(AsyncRequest asyncRequest, String result) {
                    if (asyncRequest != null) {
                        try {
                            result = asyncRequest.get();
                            Log.e(TAG, "" + result);
                            JSONObject jsonObject1 = new JSONObject(result);
                            if (jsonObject1.getString(STATUS).equals(Constants.STATUSES.Success.name())) {
                                getSavedPref().saveBoolean(SENT_TOKEN_TO_SERVER, true);
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }).execute();
        }
    }
    public void normalNotificationType() {
        /*if (new ReadPref(MainActivity.this).keyExixst(NOTIFICATION_MESSAGE_ForPop)) {
            newNotifyMessage = new ReadPref(this).getStringValue(NOTIFICATION_MESSAGE_ForPop);
        }
        new SavePref(this).remove(NOTIFICATION_MESSAGE_ForPop);*/
        newNotifyMessage=Notification_message;
        final Dialog dialog2 = new Dialog(this);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog2.setCanceledOnTouchOutside(false);

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.normal_notification_type, null, false);
        TextView notification_text = (TextView) view.findViewById(R.id.notification_text);

        notification_text.setText(newNotifyMessage);
        dialog2.setContentView(view);

        Button dismiss_dialog2 = (Button) view.findViewById(R.id.Dismiss);
        dismiss_dialog2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog2 != null) {
                    dialog2.dismiss();
                }
            }
        });
        dialog2.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(REGISTRATION_COMPLETE));

        if (checkPlayServices()) {
            if (!sharedPreferences
                    .getBoolean(SENT_TOKEN_TO_LOCAL, false)) {
                Intent intent = new Intent(this, RegistrationIntentService.class);
                startService(intent);
            } else {
                regID = sharedPreferences.getString(REGISTRATION_ID, "");
                sendTokenToServer();
                Log.i(TAG, "Registration ID from Local ------  " + regID);
            }
        }
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.app_name) + " won't run unless you update Google Play services")
                        .setTitle("Update Google Play services");

                builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        final String appPackageName = "com.google.android.gms";
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.setCancelable(false);
                dialog.show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void enableMenu() {
        radioGroup.setEnabled(true);
        rbHome.setEnabled(true);
        rbEvent.setEnabled(true);
        rbShiurim.setEnabled(true);
        most_recent_shiurim.setEnabled(true);
        rbContact.setEnabled(true);
        rbDonate.setEnabled(true);
    }

    @Override
    public void disableMenu() {
        radioGroup.setEnabled(false);
        rbHome.setEnabled(false);
        rbEvent.setEnabled(false);
        rbShiurim.setEnabled(false);
        most_recent_shiurim.setEnabled(false);
        rbContact.setEnabled(false);
        rbDonate.setEnabled(false);
    }

    @Override
    public void openDrawer() {
    }

    @Override
    public void updateCategoryList() {

        listAdapter.notifyDataSetChanged();
    }
    public SavePref getSavedPref() {
        return new SavePref(this);
    }
    public ReadPref getreadPref() {
        return new ReadPref(this);
    }
    public Context getContext() {
        return this;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSearch:
                layoutToolbar.setVisibility(View.GONE);
                searchView.setVisibility(View.VISIBLE);
                searchView.setIconified(false);
                searchView.setOnCloseListener(new SearchView.OnCloseListener() {
                    @Override
                    public boolean onClose() {
                        layoutToolbar.setVisibility(View.VISIBLE);
                        searchView.setVisibility(View.GONE);
                        if (!title.isEmpty()) {
                            Fragment fragment = viewPagerAdapter.getItem(viewPager.getCurrentItem());
                            if (fragment instanceof shiurimFragment_1) {
                                title = "";
                                ((shiurimFragment_1) fragment).setPageNum(1);
                                ((shiurimFragment_1) fragment).setCategoryID("");
                                ((shiurimFragment_1) fragment).loadData(title);
                            }
                        }
                        return false;
                    }
                });
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        Fragment fragment = viewPagerAdapter.getItem(viewPager.getCurrentItem());
                        if (fragment instanceof shiurimFragment_1) {
                            title = query;
                            ((shiurimFragment_1) fragment).setPageNum(1);
                            ((shiurimFragment_1) fragment).setCategoryID("");
                            ((shiurimFragment_1) fragment).loadData(query);
                        }
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        return false;
                    }
                });
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (getSupportFragmentManager().findFragmentById(R.id.container) instanceof ListFragment) {
            getSupportFragmentManager().findFragmentById(R.id.container).onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter{
        public Context context;
        EventsFragment eventsFragment = EventsFragment.newInstance(notificationOther);
        WebviewFragment homeFragment = WebviewFragment.newInstance(HOME);
        private shiurimFragment_1 shiurimFragment1 = shiurimFragment_1.newInstance(Constants.ACTIONS.getShiurim.name(), mp3Id, Selected_title);
        private shiurimFragment_1 most_sherium = shiurimFragment_1.newInstance(Constants.ACTIONS.getShiurim.name(), mp3Id, Selected_title);
        DonateFragment donateFragment = DonateFragment.newInstance("", "");
        Contact_Fragment contactFragment = Contact_Fragment.newInstance("Contact");

        public ViewPagerAdapter(FragmentManager fm, Context mContext) {
            super(fm);
            this.context = mContext;
        }
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return homeFragment;
                case 1:
                    return shiurimFragment1;
                case 2:
                    return eventsFragment;
                case 3:
                    return contactFragment;
                case 4:
                    return donateFragment;
                case 5:
                    return most_sherium;
                default:
                    return homeFragment;
            }
        }
        @Override
        public int getCount() {
            return 6;
        }
    }
}
