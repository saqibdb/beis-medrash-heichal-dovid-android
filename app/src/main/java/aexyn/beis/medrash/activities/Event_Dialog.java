package aexyn.beis.medrash.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import java.util.ArrayList;
import java.util.List;
import aexyn.beis.medrash.R;
import aexyn.beis.medrash.fragments.EventsFragment;
import aexyn.beis.medrash.model.EventModel;

public class Event_Dialog extends AppCompatActivity {

    ViewPager viewPager;
    EventsFragment.ImageAdapter imageAdapter;
    List<EventModel> eventModelListFinal;
    static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event__dialog);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        activity = Event_Dialog.this;

        Intent intent=getIntent();
        String EventPosition=intent.getStringExtra("SelectedPosition");
        Bundle args = intent.getBundleExtra("LIST");
        ArrayList<EventModel> object = (ArrayList<EventModel>) args.getSerializable("ArrayList");

        eventModelListFinal= new ArrayList<>();

        eventModelListFinal=object;
        viewPager = (ViewPager) findViewById(R.id.viewPager_dialog);

        LinearLayout dismiss_dialog = (LinearLayout)findViewById(R.id.clicklayout);
        dismiss_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        showEvent(Integer.parseInt(EventPosition));
    }
    public void showEvent(int position) {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                try {
                    EventsFragment.ViewPagerOnPageSelected_Dialog(position);
                } catch (NullPointerException e) {


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        try {
            setAdapter(position);
        } catch (NullPointerException e) {


        } catch (Exception e) {

        }
    }

    void setAdapter(int position) {
        try {
            imageAdapter = new EventsFragment.ImageAdapter(getSupportFragmentManager(), eventModelListFinal);
            viewPager.setAdapter(imageAdapter);
            viewPager.setCurrentItem(position);
        } catch (NullPointerException e) {

        } catch (Exception e) {

        }
    }
}
