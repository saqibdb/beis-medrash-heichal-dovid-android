package aexyn.beis.medrash.asynctask;

import android.util.Log;
import java.io.IOException;
import aexyn.beis.medrash.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SendDataOKHttp {

    static String json = "";
    String TAG = "SendDataOKHttp";
    OkHttpClient client = new OkHttpClient();

    public String postData(RequestBody formBody) {

        Request request = new Request.Builder()
                .url(BuildConfig.apiDomain)
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")
                .post(formBody)
                .build();


        try {
            Response response = client.newCall(request).execute();
            try {
                json = response.body().string().trim();
            } catch (IOException e) {
                Log.e("SendDataOkHttp", "Body Exception ---- > " + e.getMessage());
            }

        } catch (IOException e) {
            //e.printStackTrace();
            Log.e("SendDataOkHttp", "IOException --- >  " + e.getMessage());
        }
        return json;
    }

    public String getData(final String url) {

        Log.d(TAG, "URL - > " + url);

        Request request = new Request.Builder()
                .url(url)
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")
                .get()
                .build();


        try {
            Response response = client.newCall(request).execute();
            try {
                json = response.body().string().trim();
                Log.e("SendDataOkHttp", "Response ---- > " + response.toString());
            } catch (IOException e) {
                Log.e("SendDataOkHttp", "Body Exception ---- > " + e.getMessage());
            }

        } catch (IOException e) {
            //e.printStackTrace();
            Log.e("SendDataOkHttp", "IOException --- >  " + e.getMessage());
        }
        return json;
    }

}