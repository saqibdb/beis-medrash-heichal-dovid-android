package aexyn.beis.medrash.asynctask;

import android.os.AsyncTask;
import android.util.Log;
import okhttp3.RequestBody;


public class AsyncRequest extends AsyncTask<String, Void, String> {
    RequestBody formBody;
    String url = "";
    boolean isGet = false;
    private OnTaskCompleted listener;

    public AsyncRequest(String url, RequestBody formBody, boolean isGet, OnTaskCompleted listener) {
        this.formBody = formBody;
        this.listener = listener;
        this.isGet = isGet;
    }

    @Override
    protected String doInBackground(String... strings) {
        if (!isGet) {
            return new SendDataOKHttp().postData(formBody);
        } else {
            Log.i(AsyncRequest.class.getName(), "URL - " + url);
            return new SendDataOKHttp().getData(url);
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Log.i(AsyncRequest.class.getName(), "Result - " + s);
        listener.onTaskCompleted(this, s);
    }

    public interface OnTaskCompleted {
        void onTaskCompleted(AsyncRequest asyncRequest, String result);
    }

}
