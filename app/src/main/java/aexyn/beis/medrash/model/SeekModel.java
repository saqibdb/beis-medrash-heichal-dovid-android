package aexyn.beis.medrash.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ahuja on 15/4/17.
 */

public class SeekModel extends RealmObject {

    @PrimaryKey
    private int id;

    private int duration = 0;

    public SeekModel(int id, int duration){
        this.id = id;
        this.duration = duration;
    }

    public SeekModel(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public String toString()
    {
        return "SeekModel [id = "+id+", duration = "+duration+"]";
    }
}
