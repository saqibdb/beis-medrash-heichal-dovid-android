package aexyn.beis.medrash.model;

/**
 * Created by dell on 25-07-2017.
 */

public class Sherium_Search_Model {


    String Search_id, Search_Name;

    public Sherium_Search_Model() {

    }

    public Sherium_Search_Model(String _Search_id, String _SearchName) {
        Search_id = _Search_id;
        this.Search_Name = _SearchName;
    }

    public void setSearchId(String id) {
        this.Search_id = id;
    }

    public String getSearchId() {
        return Search_id;
    }

    public void setSearch_Name(String Search_Name) {
        this.Search_Name = Search_Name;
    }


    public String getSearch_Name() {
        return Search_Name;
    }

}
