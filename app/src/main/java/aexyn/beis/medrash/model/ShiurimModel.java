package aexyn.beis.medrash.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ahuja on 16/4/17.
 */

public class ShiurimModel extends RealmObject implements Parcelable{

    public static final Creator<ShiurimModel> CREATOR = new Creator<ShiurimModel>() {
        @Override
        public ShiurimModel createFromParcel(Parcel in) {
            return new ShiurimModel(in);
        }

        @Override
        public ShiurimModel[] newArray(int size) {
            return new ShiurimModel[size];
        }
    };
    private String topic ="";
    private String category ="";
    @PrimaryKey
    private int id = 0;

    private int duration = 0;

    private String title ="";

    private String speaker="";

    private String yamim ="";

    private int categoryId = 0;

    private String mp3 ="";

    private String parsha ="";

    private int fileSize = 0;

    public ShiurimModel(){}

    protected ShiurimModel(Parcel in) {
        topic = in.readString();
        id = in.readInt();
        duration = in.readInt();
        title = in.readString();
        speaker=in.readString();
        yamim = in.readString();
        categoryId = in.readInt();
        mp3 = in.readString();
        parsha = in.readString();
        category = in.readString();
        fileSize = in.readInt();
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTopic ()
    {
        return topic;
    }

    public void setTopic (String topic)
    {
        this.topic = topic;
    }

    public int getId ()
    {
        return id;
    }

    public void setId (int id)
    {
        this.id = id;
    }

    public int getDuration()
    {
        return duration * 1000;
    }

    public void setDuration(int duration)
    {
        this.duration = duration;
    }

    public int getDurationMinutes() {
        return duration;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }


    public String getspeaker ()
    {
        return speaker;
    }

    public void setspeaker (String speaker)
    {
        this.speaker = speaker;
    }

    public String getYamim ()
    {
        return yamim;
    }

    public void setYamim (String yamim)
    {
        this.yamim = yamim;
    }

    public int getCategoryId ()
    {
        return categoryId;
    }

    public void setCategoryId (int categoryId)
    {
        this.categoryId = categoryId;
    }

    public String getMp3 ()
    {
        return mp3;
    }

    public void setMp3 (String mp3)
    {
        this.mp3 = mp3;
    }

    public String getParsha ()
    {
        return parsha;
    }

    public void setParsha (String parsha)
    {
        this.parsha = parsha;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [topic = "+topic+", id = "+id+", duration = "+"duration"+", title = "+title+", speaker="+speaker+",yamim = "+yamim+", categoryId = "+categoryId+", mp3 = "+mp3+", parsha = "+parsha+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(topic);
        parcel.writeInt(id);
        parcel.writeInt(duration);
        parcel.writeString(title);
        parcel.writeString(speaker);
        parcel.writeString(yamim);
        parcel.writeInt(categoryId);
        parcel.writeString(mp3);
        parcel.writeString(parsha);
        parcel.writeString(category);
        parcel.writeInt(fileSize);
    }
}
