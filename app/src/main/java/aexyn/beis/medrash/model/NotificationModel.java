package aexyn.beis.medrash.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ahuja on 15/4/17.
 */

public class NotificationModel extends RealmObject{
    @PrimaryKey
    private int id;

    private String title;

    private String description;

    private String date;

    public int getId ()
    {
        return id;
    }

    public void setId (int id)
    {
        this.id = id;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getDate ()
    {
        return date;
    }

    public void setDate (String date)
    {
        this.date = date;
    }

    @Override
    public String toString()
    {
        return "NotificationModel [id = "+id+", title = "+title+", description = "+description+", date = "+date+"]";
    }
}
