package aexyn.beis.medrash.model;

/**
 * Created by dell on 07-07-2017.
 */

public class Mp3_notificationModel {
    private String id;
    private String title;
    private String description;
    private String date;


    public Mp3_notificationModel(){}


    public void setMp3_ID(String MP3_ID) {
        this.id = MP3_ID;
    }

    public String Mp3Get_ID() {
        return id;
    }

    public void setMp3_title(String MP3_title) {
        this.title = MP3_title;
    }

    public String Mp3Gettitle() {
        return title;
    }

    public void setMp3_description(String MP3_description) {
        this.description = MP3_description;
    }

    public String Mp3Getdescription() {
        return description;
    }


    public void setMp3_date(String MP3_date) {
        this.date = MP3_date;
    }

    public String Mp3Getdate() {
        return date;
    }
}
