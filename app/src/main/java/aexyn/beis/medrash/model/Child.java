package aexyn.beis.medrash.model;

/**
 * Created by ahuja on 19/4/17.
 */

public class Child {
    private Child[] child;

    private int id;

    private String name;

    public Child[] getChild ()
    {
        return child;
    }

    public void setChild (Child[] child)
    {
        this.child = child;
    }


    public int getId ()
    {
        return id;
    }

    public void setId (int id)
    {
        this.id = id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "Child [child = "+child+", id = "+id+", name = "+name+"]";
    }


}
