package aexyn.beis.medrash.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ahuja on 15/4/17.
 */

public class EventModel extends RealmObject implements Parcelable {
    public static final Creator<EventModel> CREATOR = new Creator<EventModel>() {
        @Override
        public EventModel createFromParcel(Parcel in) {
            return new EventModel(in);
        }

        @Override
        public EventModel[] newArray(int size) {
            return new EventModel[size];
        }
    };
    int weekDiff = 0;
    @PrimaryKey
    private int id;
    private String title;
    private String description;
    private String image;
    private String date;
    private long timeStamp;


    public EventModel() {
    }

    protected EventModel(Parcel in) {
        weekDiff = in.readInt();
        id = in.readInt();
        title = in.readString();
        description = in.readString();
        image = in.readString();
        date = in.readString();
        timeStamp = in.readLong();
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getId ()
    {
        return id;
    }

    public void setId (int id)
    {
        this.id = id;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getImage ()
    {
        return image;
    }

    public void setImage (String image)
    {
        this.image = image;
    }

    public String getDate ()
    {
        return date;
    }

    public void setDate (String date)
    {
        this.date = date;
    }

    public int getWeekDiff() {
        return weekDiff;
    }

    public void setWeekDiff(int weekDiff) {
        this.weekDiff = weekDiff;
    }

    @Override
    public String toString()
    {
        return "EventModel [id = "+id+", title = "+title+", description = "+description+", image = "+image+", date = "+date+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(weekDiff);
        parcel.writeInt(id);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(image);
        parcel.writeString(date);
        parcel.writeLong(timeStamp);
    }
}
